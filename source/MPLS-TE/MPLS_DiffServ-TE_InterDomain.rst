.. network documentation master file
   sphinx-quickstart on Sat May  2 13:48:41 2015.
   This document is mainly based on the book MPLS-enabled applications - Ina Minei, Julian Lucek
   specially on chapter 4 and 5

MPLS DiffServ-TE
-----------------------------------

.. sectionauthor:: Buez Olivier <olivier.buez@student.uclouvain.be>

Introduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adding DiffServ to MPLS-TE make it aware of a per-traffic-type quality of service. The goal here is to divide the network resources between all the types of traffic so that we limit the allocated resources for a specific type of traffic.

This allows us to guarantee some strict quality of service over a MPLS-TE network for a specific type of traffic such as voip. Where PLS-TE makes bandwidth reservation, MPLS DiffServ-TE makes separate per type of traffic bandwidth reservation. We can see roughly the networks divided into parts which each parts corresponds to a type of service where MPLS-TE runs (almost independently).

Class types
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In a DiffServ-TE, each type of traffic is divided into 8 class types called CT together with a priority of 8 levels per CT. Thus we have 64 different CT-levels classes. The CT0 with the lowest priority are often dedicated for an non guarantee service best effort like ip.

The other CT are administratively defined, for example we can have CT7 dedicated for voip where some calls has a higher priority.

Bandwidth constraints models
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As MPLS DiffServ-TE divides the total network resources between different type of service (class type CT), there are multiple ways to do such a division :

- The maximum allocation model (**MAM**)
- The Russian dolls model (**RDM**)

The maximum allocation model
"""""""""""""""""""""""""""""""""""

The most intuitive bandwidth division where a proportion of the total bandwidth is allocated for each CT. For example, we could have a scenario where 10% of the bandwidth is allocated for the voip and where data transmission has 90% allocated.

The main problem with this model is that we cannot share the unused bandwidth between CTs. For example, if we have a link of 10mbps in the scenario above, and the data transmission is already taking 9mbps, there is no way of adding 1mbps data traffic even if there is no voip traffic.

This model can face with wasted bandwidth but imposes hard traffic isolation between CTs, bring a quite good quality of service without an efficient usage of bandwidth.

.. image:: MAM_model.png
	:align: center
.. centered:: *MPLS-enabled applications - Ina Minei, Julian Lucek*


The Russian dolls model
"""""""""""""""""""""""""""""""""""

This model allow the sharing of resources between CTs to use the bandwidth efficiently.

- The Class-type CT0 with the lowest quality of service requirements (pure best-effort) has the entire bandwidth allocated.
- The Class-type CT1 has a part of the CT0 bandwidth allocated, and so on ...

That's why we call this model the Russian dolls.

.. image:: RDM_model.png
	:align: center
.. centered:: *MPLS-enabled applications - Ina Minei, Julian Lucek*

We have less isolation between all the CTs here, but the unused resources for a voip can now be used for another service and then the bandwidth is used more efficiently.

Let's imagine a link of 10 mbps of bandwidth carrying two types of services : voip characterised by CT0 at 1mbps and data transmission CT1 which has 10mbps allocated. That means each service can carry 1mbps on the link and the rest is used for data transmission.

A inconvenient situation is when the data transmission is carrying 10mbps and we need 1mbps for the voip. In this context, pre-emption must be used otherwise, we cannot guarantee the quality of service for the voip.

+-------------------------------------------------------------------+----------------------------------------------------------------------------------------+
|MAM                                                                |RDM                                                                                     |
+===================================================================+========================================================================================+
| Bandwidth may be wasted                                           | Efficient use of bandwidth                                                             |
+-------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| Useful in network where pre-emption is precluded                  | Bad in networks where pre-emption is precluded                                         |
+-------------------------------------------------------------------+----------------------------------------------------------------------------------------+
| Achieves good isolation between CTs and ensure quality of service | No isolation between CTs and ensure quality of service if pre-emption is not precluded |
+-------------------------------------------------------------------+----------------------------------------------------------------------------------------+

Path computation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In MPLS-TE, paths are computed under defined constraints such as bandwidth. With DiffServ-TE, the path computation of a specific traffic must satisfies constraints based on the available resources related to the specific type of traffic.

That means the available bandwidth per CT at all priority levels must be know for each link. The IGP must spreads the link-state together with the available bandwidth per CT at all priority levels. As mentioned earlier, there are 8 CT and 8 priority levels, giving a total of 64 different value.

The IETF decided to limit the number of advertised CT in the link-state packet to 8 different values. To do so, each routers chooses 8 CT-levels values called TE to be advertised.

Path Signalling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The calculated paths are signalled through RSVP-TE that allow establishing path and reserve bandwidth in a specific CT. Of course the CT is specified in the RSVP-TE message for a LSP, if no one is defined CT0 will be used which correspond to the lower quality of service.

After a LSP is accepted, the node computes the remaining resources and spreads the information with IGP.

Overbooking
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There is an inefficient use of bandwidth when the actual bandwidth usage is lower than the reservation. To counter this, there are four mains solutions :

- LSP size overbooking
    This method is achieved by reserving a lower bandwidth in the LSP than actually wanted.
- Link size overbooking
    This method is achieved by increasing artificially the maximum available bandwidth advertised by IGP.
- Local overbooking multipliers
    Link the Link size overbooking but specific for each CT.
- Manual configuration of the BC
    The user can manually overbook his bandwidth constraint.

The DiffServ in DiffServ-TE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. See 1.3.1 Forwarding plane mechanisms from MPLS-enabled applications - Ina Minei, Julian Lucek

After reserving the network resources among the network with DiffServ-TE, we must tag traffic to fit in the corresponding LSP related to its CT.

The DSCP that is encoded in 6 bits is used to determine an IP packet behaviour at a particular node of the network. The EXP field that is encoded in 3 bits in a MPLS header is used for such a feature in case of MPLS traffic. But we need a way to map these 6 DSCP bits which have 64 different value to a MPLS traffic which actually have only 3 bits available. Multiple solution exist for that :

- Set the EXP bits at the LSP ingress
    If the network only support up to 8 different PHB, we can only use the EXP field.
- Use EXP to determine the drop behaviour and use the MPLS label to determine the scheduling behaviour at each hop
    All of the PHB in IP packet can then be set in any MPLS flow by using different label. The solution can be seen as extending the EXP bits by using some in the Label field.

A combination of the two seems also to be possible.


Interdomain Traffic Engineering
-----------------------------------

.. sectionauthor:: Buez Olivier <olivier.buez@student.uclouvain.be>

In this section, I'm mixing with area and domain. Note that establishing LSP across area or domain are different problems. Area and domain impose the problem of visibility but domain implies another constraints about some security concerns. Moreover, multiple domains could have different ways of working. Here, we are focusing on setting LSP without visibility and IGP information sharing.

Introduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Previously, we've seen how to initiate LSP-TE through a single domain, in this part we are briefly going to discover the mechanisms that allow initiating LSP through multiple domains that is called inter-domain.

Initiating LSP-TE through multiple domains is specially useful in case of traffic engineering where we want a communication channel with some strict quality of service. All LSP are implicitly called LSP-TE here.

The main purpose of doing such a thing is the capability of setting a VPN tunnel for example through two end-point that does not belong to the same domain.


Setting up inter-domain LSP-TE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The limitations of LSP-TE to a single area is due to the visibility. No area want to show its network topologies to the others, so any collaboration can be seen between two IGP from two different areas. However, without any extension, it's possible to compute and signal an LSP-TE across multiple domain but with an off-line tool that can be seen as a manual configuration.

Firstly, I'll show the path setup and then the path computation

Path setup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To counter this visibility limitation, there are multiple ways of performing a LSP-TE across multiple area.

Contiguous LSP
"""""""""""""""""""""""""""""""""""

It's the most intuitive solution where signalling crosses domains end-to-end. All the domains are seen as a single one and the solution look like a standard TSP-LE but actually crosses multiple domains

.. image:: contiguous_lsp.png
	:align: center
.. centered:: *MPLS-enabled applications - Ina Minei, Julian Lucek*

LSP stitching
"""""""""""""""""""""""""""""""""""

The objective here is to create a path that crosses domains. Such a path is build by creating independent and consecutive LSP inside each area. The computation of the LSP is done locally to the AS from the ingress router to the egress router and the border router does the glue between the two different LSP from the two different areas so that a path can be created.

This implies no IGP information sharing between AS which is impossible and no manual configuration. That seems to be trivial and a nice solution.

.. image:: lsp_stitching.png
	:align: center
.. centered:: *MPLS-enabled applications - Ina Minei, Julian Lucek*

Something that must be noted is that the end-points of a LSP in a transit domain are the border routers .

The re-optimization (the operation to find a better LSP) and the fast reroute (backup link in case of failure) are done locally in each domain and are not involved in any higher multiple domain scope.

This solution implies some scalability issues, indeed each border routers has the work to maintain the state of all the LSP that crosses the area. To counter this scalability issue, there exists another model of setting LSP through multiple area.

LSP nesting
"""""""""""""""""""""""""""""""""""

This solution can be seen as a tunnel set in the transit domain with a LSP called FA LSP so that all end-to-end LSP are tunnelled inside. It's a LSP inside another LSP, the transit domain can get rid of maintaining the state of all crossed LSP.

.. image:: lsp_nesting.png
	:align: center
.. centered:: *MPLS-enabled applications - Ina Minei, Julian Lucek*

from a MPLS point of view, the transit of all the LSP are done by stacking and poping label.

The main differences between LSP nesting and stitching is that LSP are often used for traffic engineering purpose. In LSP stitching, the bandwidth requirements can be easily satisfied because the LSP pass through the domain natively.

In LSP nesting, many LSP can be mapped to the LSP tunnel so that some bandwidth requirements may not be satisfied. With this last solution, some admission and policing control must be introduced to make the channel QoS reliable.

We can imagine a scheme where multiple domains use different solution (LSP nesting or LSP stitching) to build the LSP end-to-end.

Path computation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The computation of the path depends on 2 main factors :

- Scope of the computation
    The scope is limited by the visibility of the network topologies. As mentioned earlier, there is no collaboration between IGP so that the computation are often performed per- locally rather than inter-domain.

- Ownership of the computation
    The computation can be done by an off-line tool or a border router at the head end. The visibility of the ownership of the computation affects the ability to compute a path.

having a global visibility across all domains are not essential to performed a calculation.

The computation can be done independently and locally in each domains. Assuming that the end-point can be know by anyway (IP reachability, ...) the border router is able to compute a path towards the domain exist and add it to the Explicit Route Object (ERO) that is called ERO extension as the LSP is signalled.

Per-domain computation involves some compatibility issues between AS. Here, the objective is to set a LSP-TE that guarantee some quality of service. The two administration from two different AS could classify traffic type to different CT.

The first one could map voice to CT0 whereas the second map to CT1. There is the same problem with link colouring. So they must agree on a mapping based on their configuration, each border router should then translate the configuration

Per-domain path computation
"""""""""""""""""""""""""""""""""""

Used when no visibility across all domains is possible. the computation are done locally per domain, the computed LSP is from one border router to the next and the rest of the computation is passed to the next AS until the destination. The computed path end-to-end is not necessary the optimal one due to the division of the computation between AS.

The work of traffic engineering is performed locally in each domain and no traffic engineering are done in a higher scope due to a lack of visibility of course.

An interesting thing is when a congestion occurred between two AS on the link of the respective border router. As the IGP doesn't spread the information of such a link, that kind of inconvenient will be difficult to overlap. To counter this limitation, IGP should include in their database the link of the boundaries.

Crankback
"""""""""""""""""""""""""""""""""""

When a failure occurred in a specific domain, a message come back to the head end that let it to compute another path, but how is this manage in a inter-domain LSP-TE scheme ?

If a failure occurred in a specific AS, it is not useful to propagate the failure to the head-end because the new computed path will be the same as the computation is done locally in each domain.

It makes sense that the failure message must be forwarded to the node that can put it to good use, for example the border router that compute the path of its own domain. If no solution can be found, the error is thrown to the previous router in the previous domain to the head-end. This process called crankback is actually a popular techniques.

If there are two blocking resources and no solution can be found, the previous border router could toggle between to path blocked due to the two blocking resources. To avoid this situation, the failure message can include an information to notify the border router to stop his calculation.

So, the failure message should include :

- the location of the failures to exclude it from a any new computation
- the information whether to continue a new computation or not

IETF include some some RSVP extensions to support crankback reports.

Path computation element
"""""""""""""""""""""""""""""""""""

Path computation element called PCE is an entity dedicated to compute the LSP. The computation of the LSP in each domain can be performed by the PCE instead of the border router. There are multiple benefits of setting a PCE :

- Constraint communication
    Some of the constraints for a LSP must be known at the head-end in case of a LSP in a single domain. With inter-domain LSP, where the computation is done per domain, some information must be spread to all the nodes that compute the path is each domain.

- Need for extra visibility
    In some case, having extra visibility could be good for some advanced features.

- Constraint translation
    Because there are administrative differences between the AS, we must find a way to translate the constraints from one domain to another as explained earlier.

- Optimality of the solution
    To find an optimal solution, it could be a good idea to run advanced algorithm that cannot fit on a border router.

The main purpose of a PCE is computing LSPs with more information we can find on a simple border router. However a PCE could be a separate module installed on a border router. Something that must be noted is that PCE is not specific to inter-domain solutions.


Multiple choice questions
^^^^^^^^^^^^^^^^^^^^^^^^^

.. question:: MPLS-TE1
        :nb_prop: 2
        :nb_pos: 1

        1. In the Maximum Allocation Model in MPLS DiffServ-TE, there is no wasted bandwidth

        .. positive:: False

        .. negative:: True

.. question:: MPLS-TE2
        :nb_prop: 4
        :nb_pos: 1

        2. Which of the following propositions are correct :

        .. positive:: There are 64 possible class type of traffic but only 8 are advertised by IGP

        .. negative:: There are 8 possible class type of traffic and all of them are advertised by IGP

        .. negative:: There are 8 possible class type of traffic and IGP can advertised 64 different values

        .. negative:: There are 64 possible class type of traffic and all of them are advertised by IGP

.. question:: MPLS-TE3
        :nb_prop: 3
        :nb_pos: 1

        3. The difficulty of setting LSP across multiples domains is due to :

        .. positive:: lack of visibility of the other domains

        .. negative:: administrative reasons, difficulty to agree on standards

        .. negative:: the cost of such a thing is too higher

.. question:: MPLS-TE4
        :nb_prop: 3
        :nb_pos: 1

        4. The computation of an LSP-TE that crosses domains without an offline tool are done :

        .. negative:: by a single entity end-to-end.

        .. positive:: per domain, each domain compute the path from the ingress to the exit point.

        .. negative:: can only be build manually.


.. toctree::
   :maxdepth: 2
