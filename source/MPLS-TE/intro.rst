.. MPLS-TE introduction
   This document is mainly based on the *MPLS and Traffic Engineering in IP Networks* article written by Daniel O. Awduche.

Introduction to MPLS-TE
-----------------------

.. sectionauthor:: Lionel Lebon <lionel.lebon@student.uclouvain.be>

Because the previous chapter talked about MultiProtocol Label Switching, we assume that you know the basics. If it does not the case, read it without further ado.

Networking is a rapidly changing domain. As History has shown us, the number of connected devices in everyday life has exploded years ago and these one need more and more flow through the network to run. Experts say that the rate at which the Internet was used by people is the most impressive one, larger than the television or radio at their time. It is concluded that this has given rise to some issues in communication between these devices in the late 90's. At this time, networking was limited by poor technologies available and limited materials. Moreover, Internet traffic grows continuously while customer expectations continue to grow also: quality, reliable and efficient services (nobody wants to wait several seconds to access a service). The challenge is big like the expectancy and the stakes are high. The requirements for service had to change to correctly adapt them. The Internet Service Providers (ISP) are the first concerned with the growing consumption and they have to prove they can provide the best service ever.

As the television as largely replaced radio, we can easily imagine that the Internet will do the same with television, therefore technologies should be adapted.

For these reasons, and to prevent the potential congestion led by the steady increase in the number of connected devices, we need to find a better use of Multiprotocol Label Switching (MPLS) to improve the application. The result has to meet some goals and one of them is to minimize the congestion of the network. The solution can be an application of MPLS to Traffic Engineering (TE). MPLS-TE has to ease the packets transport through the network and does it in the most efficient way possible. Let's see how it works and what are advantages and disadvantages of this solution. Indeed, this technology has opened a lot of new doors and fills the weaknesses of its predecessor but all is not perfect.

At the very beginning of MPLS, the technology was designed only for ATM devices. Later, the model created for ATM was extended. We called this new technology : Label Switching.

In this quick introduction, we are going talk about some aspects and concepts of MPLS and TE, the basis and what are challenges to implement a such solution on the Internet. Then, we are going to discuss about the capabilities that make MPLS applicable to traffic engineering. We are going to look at the means in place to transport data from a source to a destination. Finally, we will talk about the methodology used for TE in classical IP networks. To summarize, this introduction aims to lay the foundations to the rest of the paper, which will discuss more in detail technical aspect and performance related to the implementation of MPLS-TE on the Internet.

The growth of Internet is impressive, how ISPs reacted?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Large ISP are on the frontline and have to make big changes quickly to ensure to provide the best service at any time without loss in quality and efficiency. How they responded to this issue: the Internet growth?

Actually, Internet is became a place where all is possible: learn, create, talk, imagine, education, commerce, entertainment, etc. It is an essential media for everyone: people like you and me, enterprise and even students or banks. The Internet is used to collaborate, to inform people and even share our live via social network. Because of our life-mode changing, ISPs have employed three technical instruments in addition to the existing one to adapt the current system to meet our way to use it now: network architecture, capacity expansion and the last one but not least, the traffic engineering. Let's look at these points and how ISP used them.

1. **Network architecture**: the (abstract) structure of the networks is very important to delivery a good quality service. Having the right components that the network needs is the first step to implement the solution. Indeed, with a adequate materials, the scalability of the network will be larger. In addition, it is like a house, poor foundations are the cause of the collapse of the building in a few years. Deals with the current components and the news is imperative and pose the architecture basis. With these modifications, we can observe a greater flexibility in the packets transportation and in the networking.
2. **Capacity expansion**: this point is the logical consequence of the previous point. Because the Internet used is growing, the capacity of the network will be adapt accordingly to the consumption and before it's too late. Moreover, because users want always more, it is important to improve your technical equipments to improve the service provided (thus it is result of a increasing of bandwidth rate). It is therefore essential to do it quickly to provide a good service through the network. This will have the effect of increasing network efficiency and performance.
3. **Traffic engineering** concerns the network engineering and how improve the networks optimization. To do that, "it encompasses the application of technology, scientific principles to the measurement, modeling, characterization, and control of Internet traffic" says Daniel O. Awduche, the author of *MPLS and Traffic Engineering in IP Networks*. In addition to that, engineers need to pay attention of the techniques to improve the performance, network traffic, network utilization, etc. See the previous point for details.

Because, in the late 90's, there are a lot of engineers that is looking for the raised problems. TE search to be a solution responding to both in terms of the capacity that of the architecture and allows to improve the global performance of a network both for users en ISPs.

TE has not only advantages, at the very beginning of his implementation, there were some issues with public IP networks. Indeed, TE did not always achieve IP. The reason find his source into the limited functional of IP technologies at this time and the limitations of intradomain routing control.

The main idea is that each router uses local instantiation of a synchronized routing area link state database thanks to a independent routing decisions based on the shortest path. However, there is a  problem with IP technologies and more particularly with the measurements functions. Therefore, this approach is good but contains a flaw: during the routing decision process, protocols do not pay attention about characteristics of the network capacity constraints neither of the offered traffic through this network. The consequence is that some networks become congested while others not and that's not the goal of this technology. If you see this kind of issue, it's due to a poor resource allocation.

To conclude this section, we have introduced the TE and we are going to apply MPLS to it. This solution has been implemented by large ISP, they are moved from their hazardous technologies to a kind of standard used by service providers. So we realize that ISPs have mostly used technical resources to meet requirements. This requires that the scientific principles and new technology meet up.

MPLS for TE
^^^^^^^^^^^
There exists a framework for MPLS to resolve problem describes at the end of the previous section. This framework will be not detailed in this lecture but let's talk about the feature inside it: control capabilities in TE, based on the label swapping paradigm.

Up to now, we saw that MPLS only is not really efficient but once it is combined with differentiated services and constraint-based routing, together, they can be powerful! That's the reason of creating these kind of framework, it allows to support origination connection control through explicit label-switched paths. We discuss about that later in the paper.

Basic concepts and challenges of TE in the Internet
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Concepts
""""""""
We must start from the beginning: "a network consists of a demand system (traffic), a constraint system (interconnected network elements), and a response system (network protocols and processes)". These points are at the basis of the TE and with these concepts, we will discuss about the process model and the objectives of the TE.

1. **TE Process Model**: The process model consists of different stages, and these are iterated but the steps can be adapted:

    * Formulation of a control policy
    * Observation of the network state: monitoring functions, fault monitors and analytical models
    * Characterization and analysis stage: capacity planning, network design and network operation control
    * Optimization of network performance: capacity augmentation optimizing network performance and configuration control

.. figure:: TE-processmodel.jpg
   :alt: Process Model
   :align: center
   :width: 500

   Traffic Engineering process model - this figure comes from *MPLS and Traffic Engineering in IP Networks* article.

2. **TE Objectives**: as we said before, the main goals of the TE are the high service qualitfy, efficiency, survivability and commercial/competitive aspect and mission-critical Internet. There are two kinds of objectives. The first one is about the traffic and the second one is about the resource.

Moreover, it is important to have the "capability to compute and establish a forwarding path from one node to another" because TE "requires a precise control over the routing function".

Let's talk about the two kinds of performance objectives:

* Traffic-oriented performance is related to QoS (will be discussed later in the paper) and use metrics functions to compute packet losses, delay, delay variation and goodput. Theses results are monitored and when they reach a certain level - too low - it's time to update the materials. It's the subject of the next point, the resource-oriented performance.
* Resource-oriented performance is related about the network consumption, network utilization, how it can be improved and where it can be allocated.

Finally, these concepts lead to the conclusion that TE focuses mainly on the performance of the network utilization. However, the main goal of TE is minimizing congestion through the network.Often, congestion is due to a performance issue or resource. There are other reasons such as too much demand for the network, an inadequate and inefficient resources/structure in relation to the demand on the network.

Another objective is to obtain a reliable network, so TE search to ensure at any moment a good connection between nodes if a network failure occurs thanks to alternative path and redundant network capacity.

Challenge
"""""""""
As we said, challenge is big because of Internet consumption growing. This rapid growth generate a lot of issues (in a short and medium time level). Adapt constantly the resources is appeared to be impossible mission and for cost reasons. Moreover, the number of connected device is continuing to grow significantly and the usage of Internet is changing.

And next?
"""""""""
The following sections concerned Traffic Engineering but with some improvements to the basic model of TE. We are going to discuss about the Overlay model, Origination connection control and the MPLS traffic engineering model.

TE with with overlay model
^^^^^^^^^^^^^^^^^^^^^^^^^^
*The following paragraphs of this section are for information only. What is explained there was not seen during the lecture.*

Actually, the overlay model is a good solution to override issues explained at the end of the first section: this solution add a secondary technology called virtual circuit. Actually, it is a network build on another with interconnections between them. In more details, with the TE, a virtual circuit is add into the IP infrastructure in the configuration. The virtual circuit serve as point-to-point links between routers.

Thanks to the overlay model, it is possible to simulate a full mesh topology with some routers. The next figure illustrates a example with switches with the overlay model. As you can easily see, the core is surrounded by others routers creating routers logically interconnected by ATM Permanent Virtual Connections (PVC).

.. figure:: TE-overlay.jpg
   :scale: 100 %
   :alt: Overlay model
   :align: center

   Traffic Engineering process model  - this figure comes from *MPLS and Traffic Engineering in IP Networks* article.

It is possible to put in place other topologies thanks to a certain approach that allows to extend the design. Therefore, it is possible to use the overlay model to superimpose the physical network topology in place. Moreover, the overlay model fix the issue about the measurement because it allows to make some estimation of the traffic from statistics on the chosen PVC topology, the one that interconnect different routers. Thus, the overlay model can change and superimpose the physical network topology.

Up to now, we talked about the overlay model but this solution has several drawbacks:

* "Need to build and manage two networks": indeed, we need at least two distinct technology to put in place a such solution.
* "Increase the complexity of network architecture"
* Reliability of the system is less sure because of choices of network actors since they used path based on a series model.
* Scalability of the system is concerned too. Actually, the number of possible issue increases depending upon the routers number. This is result of a growing consumption of resources like CPU for routers.
* There exists other issue but less important. They are based on quantization and encapsulation, it concerns the electronic devices, routing instability or even the PVC failures.

The solution envisaged is to improve the code IP networks. The next figure illustrates the evolve and the expectation of the layer:

.. figure:: TE-layer.jpg
   :scale: 100 %
   :alt: Ip Layer evolution
   :align: center

   IP layer evolution over time - this figure comes from *MPLS and Traffic Engineering in IP Networks* article.

As we can see, this evolution is possible because of new technologies and the MPLS developments. The changes were made over time and with the means of the time (ATM, DWDM, OTN, etc.) to get today with "IP with MPLS over an adaptation layer interfacing with a versatile optical transport network (OTN)".

To summarize the overlay model, the IP network uses virtual circuit over the current network in place.

Capabilities that make MPLS applicable to traffic engineering
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
MPLS allows routing control capabilities and supports origination connection control (through explicit label-switching paths aka LSP) to introduce these capabilities into IP networks. Origination connection control allows to LSP to be an explicit one (explicit LSP) if the route path is resolved at the origination node.

Let's talk about Origination connection control. It allows "explicit routes to be established which are independent of the destination-based IP shortest path routing model". Thus, a LSP is installed from a signaling protocol from the route previously determined. Thanks to the circuit switching (that is superimposed on the IP routing model), the quality is increase, costs are reduced and delay are lower. This solution, compared to the overlay model, makes simpler the network architecture and the network design. As the overlay model, the core of IP networks is modified because MPLS is coupled with other services and constraint-based routing.

Another feature of the TE is the traffic trunk concept that is nothing else that an aggregation of traffic. As we said before, it is essential to have a description of traffic. It is made possible with this feature that provides a abstract description of traffic. Although that the model improves MPLS, there exist other issues like the mapping traffic trunk. Given a certain network topology, the mapping traffic trunk is created with selection of routes for explicit LSP. This is result that we use LSP tunnel and traffic engineering tunnel to talk of traffic trunk. Let's explain the two new terms: LSP tunnel and traffic engineering tunnel. These words are used "to refer to the combination of traffic trunk and explicit LSP in MPLS".

Because the quality of service are essential, the performance of the network have to optimize. This can be done with LSP tunnels. LSP tunnels can detect some issues like a congestion problems, it can detect it and reroutes to avoid the problem in the future. Tunnels are very parametrized and the network resources can be adapted to take into account these parameters. Moreover it is possible to create several LSP tunnels between nodes. In addition, LSP tunnels allows to make some statistics on network.

Overlay methodology used for traffic engineering in classical IP networks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
An MPLS traffic engineering model, the last model presented in this introduction, follows these concepts that they are essential for MPLS control plane:

**Path Management** concerns the selection of explicit routes and the instantiation and maintenance of LSP tunnels that we have seen in the previous section, it is done by three distinct functions:

* Path selection consists into specify explicit route - defined administratively or computed automatically- for a LSP tunnel, it can be represented as a sequence of hops or even a sequence of abstracts nodes. If the route is computed automatically, it is done by a constraint-based routing. The advantage of this way to specifying routes is that it reduces the level of human intervention in case of issues.
* Path placement concerns the instantiation of LSP tunnels. The instantiation is done by a signaling protocol.
* Path maintenance consists of sustaining the established LSP tunnels. It is possible to associate a set of attributes to guide the path maintenance. Therefore, it is necessary to manipulate these attributes to modify the behavior of the LSP tunnels to cause certain actions. Inside these attributes, we can include traffics parameters, adaptivity attributes, priority attributes, preemption attributes, etc. For information, know that the traffic parameters can specify the bandwidth characteristics for example. Others like the adaptivity attributes can indicate the sensitivity of an LSP tunnel. Finally, these attributes is significant to control and command the behavior of the network and the LSP tunnels.

**Traffic Assignment** concerns the traffic and it indicates that "traffic must be assigned to an LSP tunnel once the tunnel is established". Therefore, it implies that all aspects related to the allocation of traffic to established LSP tunnels is part of the traffic assignment. Concretely it's done by two distinguish function:

* Partitioning function: it partitions the traffic incoming.
* Apportionment function: it partitions the traffic to establish LSP tunnels.

It is possible to automate the system by viewing LSP tunnels as shortcuts through the IGP domain. Moreover, it is possible to add some filtration rules to restrict traffic to a certain class. Finally, load distribution across multiple LSP tunnels is an significant issue that implies assigning weights for each LSP tunnel and apportioning traffic in relative proportion to the weights.

**Network State Information Dissemination** concerns how topology state information is distributed throughout the MPLS domain. It is done thanks to a extending IGPs to distribute information about the network in link state advertisements. Additional information can be distributed too like the maximum link bandwidth,  allocation multiplier, default traffic engineering metric, reserved bandwidth per priority class and resource class attributes. All information are used to select feasible routes for LSP tunnels.

**Network management** concerns the configuration management functions, performance and accounting management functions, and fault management functions. All these functions are useful to allow the state of managed MPLS objects like LSP tunnels. The needed to optimize performance of networks is a significant issue that every traffic engineers want to solve. This is why the TE support tools that can be interfaced with MPLS to improve the monitoring and feedback on certain data.

Multiple choice questions
^^^^^^^^^^^^^^^^^^^^^^^^^

.. question:: MPLS-TE1
        :nb_prop: 4
        :nb_pos: 1

        1. What are models that improve the MPLS concepts?

        .. positive:: Overlay model, Origination connection control and the MPLS traffic engineering model
        .. negative:: Traffic Engineering connection control, and the Overlay engineering model
        .. negative:: Traffic Engineering does not need to be improved
        .. negative:: Overlay model only

.. question:: MPLS-TE2
        :nb_prop: 2
        :nb_pos: 1

        2. At the beginning, the MPLS-TE technology was designed for Cisco devices?

        .. negative:: True
        .. positive:: False

.. question:: MPLS-TE13
        :nb_prop: 5
        :nb_pos: 1

        3. Which of them are not one of the basic concepts of TE in the Internet?

        .. negative:: Optimization of network performance: capacity augmentation optimizing network performance and configuration control
        .. positive:: Count users inside your network state to adapt your solution
        .. negative:: Characterization and analysis stage: capacity planning, network design and network operation control
        .. negative:: Formulation of a control policy
        .. negative:: Observation of the network state: monitoring functions, fault monitors and analytical models

.. rubric:: References

.. [#intro_f1] Daniel 0. Awduche, "MPLS and Traffic Engineering in IP Networks", IEEE Communications Magazine, Dec. 1999.
.. [#intro_f2] Ina Minei and Julian Lucek, "MPLS-Enabled Applications - Emerging Developments and New Technologies (Third edition)", 2011
.. [#intro_f3] Unknown, "Multiprotocol Label Switching (Wikipedia)", https://fr.wikipedia.org/wiki/Multiprotocol_Label_Switching
