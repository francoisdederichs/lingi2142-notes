Guidelines for Interdomain Traffic Engineering
----------------------------------------------
.. sectionauthor:: Simon Thibert

This section is a summary of Feamster, Borkenhagen, and Rexford's paper [FBR03]_ introducing specific guidelines for interdomain traffic engineering.

We have seen that traffic engineering with BGP is a bit tricky to set up. Three reasons (among others) can be highlighted to explain this:

	- Exhaustively testing all the possible changes to BGP routing policies is computationally intractable. Indeed, as of August 2014 a full IPv4 BGP table was already in excess of 512,000 prefixes [WikiBGP]_ (see figure below).

	- When operators make changes in routing policy, we observe an unpredictable effect on the flow of traffic due to the routing policies in other domains. In other words, a change in one of the routing policies may lead to a selection of a different best route for a prefix by some of the neighboring domains.

	- Operators' ability to tune routing policies is limited because of the restrictions in the BGP decision process, and the limited number of attributes available in BGP advertisements.

.. figure:: figures/BGP2_BGPTable.png
   :align: center
   :scale: 70

   Global BGP Table growth, 1989-2015. Attribution: By Mro (Own work) CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0), via Wikimedia Commons.

In order to cope with this inherent complexity of interdomain traffic engineering with BGP, the following subsections introduce guidelines to achieve the three *fundamental objectives* presented by Feamster et al.:

	1. **Achieving predictable traffic flow changes**

	2. **Limiting the influence of neighboring domains**

	3. **Reducing the overhead of routing changes**

.. note:: These objectives are not limited to BGP, they are also applicable to interdomain traffic engineering in general.


Achieving Predictable Traffic Flow Changes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Predicting the influence of routing policy changes on the flow of traffic through the network is a key element in the effectiveness of traffic engineering, but this predictability is intrinsically difficult to achieve for two main reasons:

	- Modifications in the import policies may cause changes in the best routes of the domain. If it is the case, the domain then sends new routing advertisements to its neighboring domains. These new routing advertisements may in turn affect the traffic entering the domain from these neighbors.

	- Inversely, small changes in the routing advertisements sent by neighboring domains may cause unintended and unpredictable modifications of the best routes of the domain.

Two guidelines are proposed by Feamster et al. to achieve predictable traffic flow changes thanks to careful modifications of the import policies.


First Guideline: Avoid Globally-Visible Changes
...............................................

:Example:

	"Suppose that a particular edge link is congested and the network operator assigns a lower local preference value to some of the routes traversing the congested link. The new import policy will remove these routes from the set of possible best routes for these prefixes, thus causing some routers to redirect traffic for these destination prefixes to a different route via a different egress link. Moving the traffic reduces the load on the congested link. However, the affected routers might advertise a new route to their eBGP neighbors, such as downstream customers, potentially causing significant changes in the volume of inbound traffic." [FBR03]_

The *first guideline* stands that network operators should minimize the impact of import policies changes on the behavior of downstream neighbors, because such changes can cause unpredictability on the traffic entering the domain from these neighbors.

**How to put the first guideline into practice?**

According to Feamster et al., network operators should only tune routing policies for prefixes for which every potential best route has the same BGP attributes (except for the next-hop IP address, of course).

We may think that this will lead to a loss flexibility but this is not the case, because operators can route traffic for a wanted destination via any subset of these advertised routes without affecting the BGP advertisements seen by neighboring ASes. We call *non-transitive attribute filtering* the technique that is used to avoid propagating routes that differ only in local attributes (for example, next-hop IP address) rather than global attributes (for example, AS path). By using non-transitive attribute filtering, downstream neighbors may not even receive a new BGP advertisement, since none of the attributes sent to eBGP neighbors have changed.


Second Guideline: Limit Reaction to Minor Changes
.................................................

:Example:
	
	"Suppose a neighboring domain D advertises a three-hop AS path "D B C" to reach a particular destination, and then later changes to the path "D A C" (this may occur for traffic engineering reasons). An import policy that sets local preference based a specific AS path "D B C" would assign a different value for a route with the path "D A C", which may cause an unintended shift in the traffic associated with the destination prefix." [FBR03]_

The *second guideline* stands that network operators should design import policies that can deal with small changes in BGP advertisements sent by neighboring domains, by avoiding policies that make fine-grained distinctions between different AS paths. 

**How to put the second guideline into practice?**

According to Feamster et al., network operators should configure import policies to assign a lower local preference value to certain routes based just on the origin AS or the AS path length (the exact sequence of ASes in the path is therefore not taken into account). However, there is a limitation to this approach: the distribution of the traffic over different lengths of AS paths influences its specific effects.


Limiting the Influence of Neighboring Domains
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Via their routing advertisements, neighboring domains generally have a great influence on the routing choices of a domain. This influence mainly takes root in two factors:

	- AS path prepending (that is, repeating an element in the AS path multiple times to artificially make the path appear longer; see figure below, inprired by [Huston06]_) is commonly used and limits the operators' ability to influence the flow of traffic.

 	- Operators' control over the network traffic flow is also reduced by the inconsistencies in the routes advertised via different eBGP sessions by a neighboring AS.

.. figure:: figures/BGP2_ASPrepending.png
   :align: center
   :scale: 80

   AS path prepending.

One guideline is proposed by Feamster et al. to limit the influence of neighboring domains on the path selection process.


Third Guideline: Limit the Influence of AS Path Length
......................................................

A shortest AS path length does not always imply a shortest network distance (see figure below, taken from [FBR03]_). Similarly, a shortest AS path length may "contain" a network that is experiencing high latency, loss, or contains many intradomain router hops.

.. figure:: figures/BGP2_ShortestPath.png
   :align: center
   :scale: 40

   A path from Boston to Washington, D.C. that traverses two intermediate ASes on the way may be shorter than a path with one intermediate AS that does not have a geographically proximal exchange point.

The *third guideline* stands that network operators should establish coarse-grained AS path length categorization in order to limit the influence of the AS path length on the comparison of routes.

**How to put the third guideline into practice?**

According to Feamster et al., network operators should allow the set of best paths to include AS paths with small differences in length. This can be achieved by disabling the "Shortest AS path" step of the BGP decision process and assigning local preference ranges based in part on AS path length (for example, assign a range of local preference values to 1-hop paths, another range to 2-hop, 3-hop, and 4-hop paths, a third range to 5-hop and 6-hop paths, and so on). This ensures that AS path length has still an influence on the decision process while restricting this influence on the comparison of routes.


Remark: Consistent Advertisements from Neighbors
................................................

:Example:

	"Suppose that a network connects to AS A at locations on the east and west coast. If AS A advertises a prefix only on the east coast, then this would force the other network to carry all of the outbound traffic for this prefix to the east coast. Alternatively, AS A might advertise the path with a different AS path length or origin type at different locations." [FBR03]_

Network operators should pay attention to the issues driven by inconstistent advertisments, since this practice can have a significant and unpredictable influence on the flow of traffic. 

.. note:: This practice is often a violation of peering agreements.


Reducing the Overhead of Routing Changes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Another key element of traffic engineering is the ability of moving a portion of the traffic in the network from one link to another. This traffic can be selected by BGP import policies based on the prefixes and the attributes in the route advertisements, but the routing changes can unfortunately cause overhead on routers.

Three guidelines are proposed by Feamster et al. to limit the overhead of routing changes on the routers.


Fourth Guideline: Group Related Prefixes
........................................

The *fourth guideline* stands that network operators should change routing policies based on characteristics in the routing advertisements (rather than on the specific prefix) in order to achieve traffic engineering goals for a significant number of prefixes with a relatively small number of policy changes (because many prefix advertisements have the same characteristics).

**How to put the fourth guideline into practice?**

According to Feamster et al., network operators should classify each prefix based on the routers where the routes for that prefix were learned, as well as the AS paths that were learned for each prefix. In other words, if several prefixes are advertised to the same set of routers and, at each router, the routes for those prefixes have the same AS paths, those prefixes are considered as having the same routing choices. 


Fifth Guideline: Focus on Popular Destinations
..............................................

The *fifth guideline* stands that network operators should focus on (groups of) prefixes that are responsible for larger fractions of the traffic (that is, popular destinations) in order to move large amounts of traffic with a small number of routing changes.

**How to put the fifth guideline into practice?**

According to Feamster et al., network operators should modify import policies based on route advertisement attributes (for example, the local preference value) in order to move the traffic associated with popular groups of related prefixes, rather than moving traffic for individual prefixes.

However, the appropriate amount of traffic to move may depend on the current link loads. Operators must therefore have a knowledge about traffic distributions for each origin AS and each set of prefixes with common routing attributes. Such knowledge can be gathered with flow-level traffic measurements, for example with Cisco's Netflow feature. We invite the reader to refer to the "Flow-Level Traffic Measurements" section of Feasmter et al.'s paper for a complete description of these measurements.


Sixth Guideline: Move Stable Traffic Volumes
............................................

Finally, the *sixth guideline* stands that network operators should focus on routes for destinations whith relatively stable traffic volumes over time.

**How to put the sixth guideline into practice?**

According to Feamster et al., network operators should track traffic measurements (in the same way as explained in the previous guideline) over time to identify the specific (groups of) prefixes with relatively stable traffic volumes.

.. note:: "The degree of stability varies across the popular destinations. (...) This amount of fluctuation is arguably small enough to enable the traffic engineering tools to make accurate predictions of the volume of traffic that would move from one route to another." [FBR03]_

