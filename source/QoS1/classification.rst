.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Packet classification
---------------------
.. sectionauthor:: Sebastien Duval & Benoît Duhoux

.. todo:: 

	* End MCQ

Before talking about the packet classification, let first explain the main idea behind the structure that is applied in this traffic control chapter:

.. figure::  pictures/traffic_control.png
   :align:   center

   Traffic Control Mechanism

See the above figure reference : [#img_src]_.

This figure (taken from Cisco) explain briefly what the *Traffic Control* chapter is about: Packet classification, `Policing and shaping <tokenbucket.html>`_, `Buffer acceptance <bufferacceptance.html>`_  and `Packet schedulers <schedulers.html>`_.

What does Classification means ?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
It is the action of categorizing packet into different classes in the network. That is, group packets based on their similarity. Thus, grouping these packets (ie classify the packets) allows us to do Quality of Service. When the traffic is classified, some classes will be processed differently from other depending on the policy that will be applied to them. Based on the policy, the treatment can vary. We can see the classification as being a partitioning into multiple priority levels.

In practice the classification occurs when packets enter the network. From there, it is defined by buckets that will map the traffic that satisfies some properties, criterias previously established. 
There are lots of different criteria. Some can be global to a layer, namely the packets can be classified depending on the layer (eg Layer 2 VLAN) or the interface. The criterias can also be more specific (classification based on a particular aspect of the traffic such as the packet size for example).

How does classification works ?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
As said previously, the classification takes place at the input of the network but could also be done on any node (but it is way more significant to do it at the network border). 

So the packets will enter the network and will be successively reviewed by different criteria in order to be classified. The classification can be done according to source or destination of packets, etc, even on the basis of the application that is referred.

.. figure::  pictures/classifier.png
   :align:   center

   Classifier architecture

In the figure above, the *Manage Terminal* is considered as the network input node and *Unmanaged Terminal* like the other network nodes.
In more details, the *Manage Terminal* are terminals where the classifiers are being specified and the *Unmanaged Terminal* will receive the packets from the manage terminal. The *Classifying Entity* shall be responsible for classifying packets coming in (IN) or going out (OUT). We call *Classifers*, groupes of attributes that specifies how to match a packet by checking one or more fields in the latter (usually in the header). 


One can for example create a classifier that will create a flow/class of packets for all packets having the same TCP/IP destination ports. We can then create classifiers that contains several criterias to identify the packets more accurately.


The different packet classification techniques
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In this section, we discuss the main classification techniques that is, on what features can the packets be classified. These techniques are ranked according to the network layer to which they belong.

Layer 1
```````

In the layer 1, we can filter/classify using the ingress interface through which the packet arrives at the router. In case of a switch, it is also possible but it need to be filtered according to the port : ingress port (port that are used to enter the switch) and egress port (port that are used to leave the router). 
There is no more to say about the layer 1 because it only focuses on the physical aspect.

Layer 2
```````

Once the packets are in, some techniques are available at the Datalink layer. A classifier can also classify the packets according to the following:

- *Mac address source/destination*: The 48 bits Mac can be used to filter any flow of frames that goes from a source Mac address to a destination Mac address or only based on the source/destination.
  
- *Class Of Service (Cos)*: The Cos is a 3 bit field defined by the IEEE 802.1p that is present in Ethernet frame headers. This field is only present when the VLAN tagging (802.1Q) is enabled. In this Class Of Service, we can distingish 8 classes (CS) that goes from the lower priority (0) to the highest (7):
  
	+------------+--------------------------------------+
	| Tag value  | Traffic type                         |
	+============+======================================+
	| 0 (000)    | Best effort                          |
	+------------+--------------------------------------+
	| 1 (001)    | Background                           |
	+------------+--------------------------------------+
	| 2 (010)    | Spare                                |
	+------------+--------------------------------------+
	| 3 (011)    | Excellent Effort                     |
	+------------+--------------------------------------+
	| 4 (100)    | Controlled Load                      |
	+------------+--------------------------------------+
	| 5 (101)    | Video (Interactive Video)            |
	+------------+--------------------------------------+
	| 6 (110)    | Voice (Interactive Voice)            |
	+------------+--------------------------------------+
	| 7 (111)    | Network Control (reserved traffic)   |
	+------------+--------------------------------------+

    + 802.1Q: this protocol is responsible for the VLAN tagging. Each VLAN is associated with a specific ID number. When a frame needs to be tagged to a VLAN, the header is extended with the following
    
	+------------------------------------------+---------------------------------------------------------------+
	| TPID (tag protocol identifier): 16 bit   | Global tag (0x8100) to signify the 802.1Q tag                 |
	+------------------------------------------+---------------------------------------------------------------+
	| Priority: 3bit                           | The 3 bits to define the priority for the tagging (see above) |
	+------------------------------------------+---------------------------------------------------------------+
	| CFI (Canonical Format identifier): 1 bit | For compatibility between Ethernet and Token Ring             |
	+------------------------------------------+---------------------------------------------------------------+
	| VLAN ID: 12 bit                          | ID used to distinguish the different VLAN on the link         |
	+------------------------------------------+---------------------------------------------------------------+


	The VLAN-ID can be used to classify frame that goes to or come from a specific VLAN on a network. The same thing can be done with the priority allowed to the tagged frames.

- *Multiprotocol Label Switching Experimental Bits (MPLS-EXP)*: Let's first explain what the MPLS-EXP bits are. The MPLS protocol has been defined with 3 bits for traffic control: priority QOS and ECN (explicit congestion Notification). The latter is develloped at the layer 3. These 3 bits were reserved for experimental purpose but not anymore. It are now know as the *Traffic Class (TC)* field in the MPLS header as announced by the `RFC 5462 <http://tools.ietf.org/html/rfc5462>`_. These bits are used to prioritize the traffic in MPLS.
  

.. - (Frame Relay Discard Eligibility bits (DE))

Layer 3
```````
The network layer also has several techniques to deal with the packet classification. Because the layer 2 media often changes as the packet goes through, we need to had more ubiquitous classification techniques that will be applied to packets on all their way from a source to a destination. This is the reason why layer 3 techniques were deployed.

- *Source/Destination address*: Classifying on the source/destination ip address can be used to filter any flow of packets that goes from a sources IP address to a destination IP address or only based on the source/destination address.

Before further devellopment, let's explain what the Tos(Traffic of service) is. The Tos is a 8 bits field in the IPv4 packet that has had various purpose over the years. In its earliest definition it was intended for IP precedence (IPP)

- *IP precedence (IPP)*: it uses the 3 first bits of the Tos field. The other bits are unused. The IP precedence allows 8 different values like the layer 2 Cos and is used to allows us to specify the class of service (for high bandwidth, low delay, etc) for a packet but the Tos field was never well-defined nor well-deployed.
  
	+------------+--------------------------------------+
	| Tag value  | Traffic type                         |
	+============+======================================+
	| 0 (000)    | Default marking value                |
	+------------+--------------------------------------+
	| 1 (001)    | For data application: medium prior   |
	+------------+--------------------------------------+
	| 2 (010)    | For data application: high prior     |
	+------------+--------------------------------------+
	| 3 (011)    | Voice Control                        |
	+------------+--------------------------------------+
	| 4 (100)    | For video conferencing and streaming |
	+------------+--------------------------------------+
	| 5 (101)    | Voice bearer                         |
	+------------+--------------------------------------+
	| 6 (110)    | Reserved for network traffic         |
	+------------+ such as routing                      |
	| 7 (111)    |                                      |
	+------------+--------------------------------------+

Afterwards the Tos field was redefined by the RFCs(`RFC 2474 <https://tools.ietf.org/html/rfc2474>`_ and `RFC 2475 <https://tools.ietf.org/html/rfc2475>`_) in order to use DSCP and ECN instead of the IP Precedence. The DSCP reused the 3 bits that were used in the IPP because any modification of those 3 bits will be considered illegal by the end-points that were used to use the IP precedence. It is a backward compatibility with the old standard that IPP was.

- *DiffServ Code Point (DSCP)*: It was implemented for the DiffServ service and especially in the Per-Hop Behavior (PHB). The DSCP uses the 6 first bits of the Tos field. Because the DSCP uses 6 bit and IP precedence only 3, some rules have been defined in order to translate one into the other and vice versa. This is simply done by the following

	+---------------+------------+----------------+-------------+
	| IPP (Decimal) | IPP (Bits) | DSCP (Decimal) | DSCP (Bits) |
	+---------------+------------+----------------+-------------+
	| 1             | 001        | 8              | 001000      |
	+---------------+------------+----------------+-------------+

	Thus to convert IPP to DSCP, we just have to multiply the value by 8.

	The DSCP can be further develloped but for this part we limit ourself to the classification.

- Explicit Congestion Notification (ECN): These are the remaining 2 bits in the Tos field. Its goal is basically to allow the notification of network congestion between end-to-end devices. Unfortunalely they are still unused in the context of DSCP.

Layer 4
```````
Let's go higher in the OSI model and define the techniques used in the layer 4, the Transport layer. Since we spoke about mac addresses in the layer 2 and IP addresses in the layer 3, we can not omit to speak about the TCP/UDP points of view for the classification.

- *TCP/UDP port*: We can create a classifier that will check the TCP or UDP port field for a source or a destination or for both (source and destination) according to a specific flow.

Classifing on the TCP/UDP port is one of the most widely used approach. Even if this technique is quite easy and a quick one to use, it has some limitations.
We cannot have an unique mapping by port because two or more applications can use the same port. Furthermore for some applications which have mutliple traffic, it will be difficult to determine the traffic by looking at the port number.

These are some of the reasons that make port-based classification quite imprecise for many of the tasks.

Layer 7
```````
We are now at the last layer, the layer 7, the Application layer. Above we described the different header field that classifier can inspect to classify the traffic. This layer focuses on the application side. This means, how can we decide to classify traffic according to the application to which it is related. In the layer 7, what is used to classify is called the Deep Packet Inspection (DPI). 

The *Deep Packet Inspection* is a technology that is capable to inspect every byte from every packet that goes though a DPI device. It can analyse packet headers, the content of the packet, types of applications, etc.  As its name suggests, the DPI goes deep into the inspection of packets. This means that, instead of looking at specific header fields of IP packets or frame, the DPI is able to look at the payload of a packet. By doing it, the DPI can retrieve information and use them to identify at the same time the application signature. Using such techniques can allow us to classify traffic according to some application signature (i.e HTTP, SMTP, SSH, etc), application type, code payloads, etc.

Some manufacturers implement their own DPI such as Cisco with their Network Based Application Recognition (NBAR).

.. We are here in the layer 7 section and it is not really the right place to devellop the DPI.
.. As a matter of fact, the DPI is capable to inspect all traffic that goes from layer 2 to 7. It is global to all the layers.

Even if the DPI can be used as a classification technique, it is also useful from a security view point. Indeed it can be used to create conditions for the network access rules or to inspect deeply a packet in order to prevent viruses or spyware to enter a network.
The DPI is supossedly to be a real time analysis that can handle a very large data steam throughput. They can then directly be put in a data stream, forcing all the traffic to go through.

The drawbacks of this techniques reside in the security. Even if the DPI can be useful to add security to a network, the DPI can be exploited to facilitate attacks like buffer overflow, DoS, etc. Furthermore DPI can add complexity to the existing firewall.


Multilayer technique
````````````````````
Access Control List (ACL): 
""""""""""""""""""""""""""""

ACL are traffic filters that will control what's crossing routers or switches. It can permit or deny some traffic according to well defined rules. It works by comparing the field of the packet it receives to its access control lists. The rule in the ACLs have to be correclty defined otherwise it can block unwanted traffic. When a packet is filtered by an ACL, it will go through each condition of the access list. When the packet matches a condition, it will be rejected of accepted according to the rule. A problem that may arise is when a rule is too general in the list. Indeed, in this case, it is likely that nearly all the packets will match the rule conditions. This make the order is very important in ACLs. 

The ACL rules can then be used to classify traffic packet based on several criteria. These criteria can belongs to different layer of the OSI model (layer 1,2,3,4). Packets can then be classified by sources/destination Mac adress and source/destination IP address and source/destination port number. This ACL can be applied globally or to some interfaces. The only layer that is not used in the ones describes above is the Application layer.

Even if the ACL can be used to classify the traffic packet, it's main purpose is to filter the traffic and thus restrict the content that is accesible. Without it, all traffic could pass the router and enter the network easily.

.. rubric:: References

Classification:

Deploying QoS for Cisco IP and Next Generation Networks: The Definitive Guide
 Par Vinod Joseph,Brett Chapman, chapitre 2
Video Systems in an IT Environment: The Basics of Professional Networked ...
 Par Al Kovalick chapitre 6.6

- https://tools.ietf.org/html/rfc5777#page-7
- http://netcerts.net/qos-classification-and-marking/
- http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus1000/sw/4_0/qos/configuration/guide/nexus1000v_qos/qos_2classification.pdf
- http://www.alliedtelesis.com/media/fount/software_reference/271/ar400/cl-4rap7.pdf
- http://www.cisco.com/c/en/us/td/docs/solutions/Enterprise/WAN_and_MAN/QoS_SRND/QoS-SRND-Book/QoSIntro.html

Layer 2

	802.1Q

- http://www.cisco.com/c/en/us/support/docs/lan-switching/8021q/17056-741-4.html
- https://www.urz.uni-heidelberg.de/Netzdienste/docext/3com/superstack/3_0/3500/3i3qos2.html

MPLS_EXP

- https://tools.ietf.org/html/rfc5462

Layer 3 

ip precedence:

- http://www.cisco.com/c/en/us/td/docs/ios/12_2/qos/configuration/guide/fqos_c/qcfclass.html
- http://www.dasblinkenlichten.com/ip-precedence-and-dscp-values/

diffserv :

- http://www.ciscopress.com/articles/article.asp?p=28688
- https://tools.ietf.org/html/rfc2474#page-7
- https://tools.ietf.org/html/rfc2873

Layer 4

- http://www.schneider-grin.ch/media/pdf/diploma_thesis.pdf

Layer 7

- http://searchnetworking.techtarget.com/definition/deep-packet-inspection-DPI
- http://www.techrepublic.com/blog/data-center/deep-packet-inspection-what-you-need-to-know/
- http://www.measurementlab.net/download/AMIfv94jg7ouv9moLMv3Hqp7C1f3lyrwqUWhk16gpjVctJSeyPtHztBznXMDFX0Y4DEJI_Gt7_yT-oFwt0jwlWHUEuTNLLdoHsEj5c-Cr5mlfq8zj5mZXl4uDZVtjBia-mS5vrLST7f62IKQrhtqMUyPiuUBGJ3x1A/
- netgroup.polito.it/Members/niccolo_cascarano/pub/tesi.pdf/at_download/file 

ACL

- http://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst3750x_3560x/software/release/12-2_55_se/configuration/guide/3750xscg/swacl.html#pgfId-1520318
- http://www.cisco.com/c/en/us/td/docs/ios/12_2/security/configuration/guide/fsecur_c/scfacls.html#wp1000893
- http://www.h3c.com/portal/Technical_Support___Documents/Technical_Documents/Switches/H3C_S9500_Series_Switches/Configuration/Operation_Manual/H3C_S9500_OM-Release1648[v1.24]-QoS_ACL_Volume/200901/624741_1285_0.htm

.. rubric:: Footnotes

.. [#img_src] This picture has been taken from the `Quality of Service Design Overview page <http://www.cisco.com/c/en/us/td/docs/solutions/Enterprise/WAN_and_MAN/QoS_SRND/QoS-SRND-Book/QoSIntro.html>`_ from Cisco.


Question 1:
```````````

.. question::
	:nb_pos: 2
	:nb_prop: 4

	Which of the following proposals match best the classification in networks:
		 
	.. negative::
	 	It is used to filter packets for Quality of service. If packets do not match some criterias, they are dropped.

	.. negative::
		It is used to monitor the traffic by tagging the packets according to specific classes

	.. positive::
	 	It is used to group specific packets into class of service in order to do Quality of service.

	.. positive::
	 	Is is used to filter packets for Quality of service. Lots of criteria exist in order to match the various aspect of the traffic. 


Question 2
``````````

.. question::
	:nb_pos: 1
	:nb_prop: 3

 	Let's imagine we want to classify the traffic according to the layer 2 techniques. We have a network using VLAN 2 and VLAN 3 for respectively voice and default traffic. Which of the following correspond to the field that need to be check in order to do the classification correctly.

 	.. positive::
 		802.1Q Priority : 110 
 		802.1Q VLAN ID : 2
 		and
 		802.1Q Priority : 000
 		802.1Q VLAN ID : 3
 		.. comment::
            		A mistake is possible with the last proposal but because it is VLAN, the Cos field is present and a priority bits are set.

 	.. negative::
 		802.1Q Priority : 110 
 		802.1Q VLAN ID : 3
 		and
 		802.1Q Priority : 011
 		802.1Q VLAN ID : 2		 		

 	.. negative::
 		802.1Q Priority : 110 
 		802.1Q VLAN ID : 2

 	.. negative::
 		802.1Q Priority : 1100000 
 		802.1Q VLAN ID : 2

Question 3
``````````

.. question::
	:nb_pos: 1
	:nb_prop: 3

	The ToS field was redefined by the RFCs to use the DSCP instead of the IPP. How many bits are used/defined in the previous and new definition of ToS field.

	.. negative::
		Previous definition : 8 bits used, 0 bit not used
		New definition :  3 bits used, 5 bits not used

	.. negative::
		Previous definition : 3 bits used, 5 bits not used
		New definition : 6 bits used, 2 bits not used.

	.. positive::
		Previous definition : 3 bits used, 5 bits not used
		New definition : 8 bits used, 0 bit not used 

.. question::
	:nb_pos: 1
	:nb_prop: 4

	The ToS field was redefined by the RFCs to use the DSCP instead of the IP. DSCP has a backward compatibility with the old standard that IPP was. If the IPP is set to be 101 what will the value of the DSCP.

	.. positive::
		101000

	.. negative::
		000101

	.. negative::
		100101

	.. negative::
		101001

Question 4
``````````

.. question::
	:nb_pos: 1
	:nb_prop: 4

	Is it possible to classify the traffic packets based on several layers ?

	.. negative::
		No because each layer act indepentently from the other when classifying

	.. negative::
		Yes only layer 2-3 because both have the same marking for their Cos/Tos fields

	.. negative::
		Yes layer 2-3-4. They are considered as the most important layers in the OSI model to deal with the traffic.

	.. positive::
		Yes all of them. All layer can be completed by the other in order to refine the filtering.

Question 5:
```````````
We want to classify the packets that have a payload lenght > 1024 bytes to a classe that we will cal X, and packets that have payload lenght > 1024 bytes to a classe that we will call Y.

.. question::
	:nb_pos: 1
	:nb_prop: 4

	Question 1.a: Which layer is responsible to classify these packets:

	.. negative::
		Layer 2

	.. positive::
	 	Layer 3

	.. negative::
	 	Layer 4

	.. negative::
	 	Layer 7


.. question::
	:nb_pos: 1
	:nb_prop: 3

	Question 1.b: In with order have we to put to classification in order to correclty put the packet to their class Of service: 
		
	.. negative::
		First we check if size_payload > 1024 then we check if size_payload > 2048. If the first is matched then the packet goes to the X class of services and if it is the second one packet will go to the Y class of service.

	.. positive::
		First we check if size_payload > 2048 then we check if size_payload > 2014. If the first is matched then the packet goes to the Y class of services and if it is the second one packet will go to the X class of service. 

	.. negative::
		Both solution are possible.

Question 6:
```````````
 
.. question::
	:nb_pos: 1
	:nb_prop: 3

	For this exercice, we will consider the ACL. For the purpose of this exercice, we are going to use the syntax of Cisco. This is not a prerequise to answer the question and not to be known. 
	Let's first take an example: 
	
		* access-list 1 permit tcp host 192.168.10.1 any eq 22
	
	which means that we create an access-list number 1 that permit the host 192.168.10.1 to do TCP connection to any address on port 22 (ssh).

	This exercice is only relevant to understand the importance of conditions in ACLs.

	Consider now the following exercice: which access-list is the most restritive to allow the user in the 192.168.10.0 network to do HTTP requests on TCP and deny the HTTPS requests on TCP.
	
	.. negative::
		access-list 1 deny tcp 192.168.10.5 any eq 22
		access-list 1 permit tcp 192.168.10.0 0.0.0.255 any eq 80
		access-list 1 deny tcp 192.168.10.0 0.0.0.255 any eq 80
		access-list 1 permit tcp 192.168.10.0 0.0.0.255 any eq 443

	.. positive::
		access-list 1 deny tcp host 192.168.10.5 any eq 22
		access-list 1 permit tcp 192.168.10.0 0.0.0.255 any eq 80
		access-list 1 permit tcp 192.168.10.0 0.0.0.255 any eq 443
		access-list 1 deny tcp 192.168.0.0 0.0.255.255 any eq 80 
		
	.. negative::
		access-list 1 permit tcp 192.168.10.5 any eq 22
		access-list 1 deny tcp 192.168.10.0 0.0.0.255 any eq 80
		access-list 1 deny tcp 192.168.10.0 0.0.0.255 any eq 443
		access-list 1 permit tcp 192.168.0.0 0.0.255.255 any eq 80 
		

