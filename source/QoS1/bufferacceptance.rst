.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Buffer acceptance
-----------------
.. sectionauthor:: Jeremy Vanhee

   Explain the main buffer acceptance algorithms, how they can be used and what are their parameters. Provide a few illustrative examples and a set of multiple choice questions. 

This section is the summary of [#ref_source]_ .

In high-speed networks, routers are likely to be designed with correspondingly large maximum queues to accommodate transient congestion. TCP transport protocol detects congestion only after a packet has been dropped. It is not a good idea to have large queues; it would increase the average delay in the network. It is now important to have mechanisms that keep throughput high and average queue size low. The detection of congestion is the most effective in the router. It can reliably distinguish queueing and propagation delay. This section proposes a congestion avoidance mechanism at the router, RED (Random Early Detection) routers, with somewhat different methods for detecting congestion and for choosing which connections to notify of this congestion. The RED router is designed for a network where a single marked or dropped packet is sufficient to signal the presence of congestion.

RED routers can be useful in routers with a range of packet-scheduling and packet-dropping algorithms (for example, where packet are marked as either 'essential' or 'optional', 'optional' are dropped first). The RED congestion control mechanisms monitor the average queue size for each output queue and choose randomly connections to notify of that congestion.

RED routers can mark a packet by dropping it at the router or by setting a bit in the packet header (depend on the protocol). If the average queue size exceeds a maximum threshold, the RED router will mark every packet that arrives.

Design guidelines
^^^^^^^^^^^^^^^^^
The main goal is to provide congestion avoidance by controlling the average queue size. A congestion avoidance scheme maintains the network in a region of low delay and high throughput. The router is the most appropriate agent to determine the size and duration of short-lived bursts in queue size to be accommodated by the router. It is done by controlling the time constants used by the low-pass filter for computing the average queue size. The second job is to choose which connection to notify of congestion at router.  Another goal in deciding which connections to notify is to avoid the global synchronization that results from notifying all connections to reduce their windows at the same time.

Congestion avoidance routers can use distinct algorithms for congestion detection and for deciding which connections to notify. The RED router uses randomization in choosing which arriving packets to mark; with this method, the probability of marking a packet from a particular connection is roughly proportional to that connection's share of the bandwidth through the router. This method can be efficiently implemented without maintaining per-connection state at the router.

The RED algorithm
^^^^^^^^^^^^^^^^^
The RED router calculates the average queue size, using low-pass filter with an exponential weighted moving average. The average queue size is compared to two thresholds, a minimum and a maximum. When the average queue size is less than the minimum threshold, no packets are marked. When the average queue size is greater than the maximum threshold, every arriving packed are marked. If marked packets are in fact dropped, or if all source nodes are cooperative, this ensures that the average queue size does not significantly exceed the maximum threshold. When the average queue is between the two thresholds, each arriving packet is marked with a probability ``p_a``, where ``p_a`` is a function of the average queue size ``avg``. The general RED router algorithm:
::
	for each packet arrival
    	calculate the average queue size avg
        if min_th <= avg max_th
        	calculate probability p_a :
            with probability p_a
            	mark the arriving packet
        else if max_th <= avg
        	mark the arriving packet
            
Thus the RED router has two separate algorithms. The algorithm for computing the average queue size determines the degree of burstiness that will be allowed in the router queue. The algorithm calculating the packet-marking probability determines how frequently the router marks packets, given the current level of congestion. The goal is for the router to mark packets at fairly evenly-spaced intervals, in order to avoid biases and to avoid global synchronization, and to mark packets sufficiently frequently to control the average queue size.

The router's calculations of the average queue size take into account the period when the queue is empty (the idle period) by estimating the number m of small packets that could have been transmitted by the router during the idle period. After the idle period the router computes the average queue size as if m packets had arrived to an empty queue during that period. 

As ``avg`` varies from ``min_th`` to  ``max_th``, the packet-marking probability ``p_b`` varies linearly from 0 to ``max_p``:

:math:`p\_b = max\_p(avg - min\_th)/(max\_th - min\_th)`

The final packet-marking probability ``p_a`` increases slowly as the count increases since the last marked packet:

:math:`p\_a = p\_b/(1-count*p\_b)`

This ensures that the router does not wait too long before marking packet.

Detailed algorithm:

.. image:: pictures/algo_RED.png
	:align: center
	

Comparing Drop Tail and RED routers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The RED router can accommodate the short burst in the queue required by TCP's slow-start phase; thus RED routers control the average queue size while still allowing TCP connections to smoothly open their windows. This figure shows the results of simulations of the network with two TCP connections, each with a maximum window of 240 packets, roughly equal to the delay-bandwidth product. The simulations compare the performance of Drop Tail and of RED routers.

.. image:: pictures/comparing_DT_RED.png
	:align: center

For this simple network, the network power is higher with RED than with Drop Tail routers. With Drop Tail routers with a small maximum queue, the queue drops packets while the TCP connection is in the slow-start phase of rapidly increasing its window, reducing throughput. On the other hand, with Drop Tail routers with a large maximum queue the average delay is unacceptably large. In addition, Drop Tail rotuers are more likely to drop packets from both connections at the same time, resulting in global synchronization and a further loss of throughput.


Calculating the average queue length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The RED router uses a low-pass filter to calculate the average queue size. Thus, the short-term increases in the queue size that result from bursty traffic or from transient congestion do not result in a significant increase in the average queue size.

The low-pass filter is an exponential weighted moving average (EWMA):

:math:`avg = (1-w\_q)avg + qw\_q`

The weight ``w_q`` determines the time constant of the low-pass filter.

WRED: Weighted random early detection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
It is an variant to random early detection (RED), sometimes called multi-RED, that drops packets selectively based on IP precedence, i.e. packets with higher IP precedence bits have a lower probability of being dropped than packets with lower precedence. 
WRED is designed for the core of a network, the packets entering the network are marked with appropriate IP precedence bits at the edge routers and, hence, have differentiated drop probabilities.

Questions
^^^^^^^^^

Question 1:
'''''''''''

.. question::
	:nb_pos: 2
	:nb_prop: 4

	What is true for the RED algorithm?
	
	.. negative::
		It prefers large queue.

	.. positive::
		It detects congestion.
		
	.. positive::
		It chooses which connections to notify of the congestion.
    
        .. negative::
                It has three separed algorithm.
        
Question 2:
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	What are the advantages of RED algorithm compared to Tail Drop?
	
	.. negative::
		The queue drops packets while the TCP connection is in the slow-start phase.

	.. negative::
		It creates global synchronization.
		
	.. positive::
		It accommodates the short burst in the queue required by TCP's slow-start phase.
        
Question 3:
'''''''''''

.. question::
	:nb_pos: 2
	:nb_prop: 3

	What is true for the WRED algorithm?
	
	.. positive::
		It drops packets selectively based on IP precedence.

	.. positive::
		It is an extention of RED algorithm.
		
	.. negative::
		Packets entering the network have the same drop probabilities.
		

        
Source
^^^^^^
.. [#ref_source] Sally Floyd and Van Jacobson, Random Early Detection Gateways for Congestion Avoidance, http://www.icir.org/floyd/papers/early.twocolumn.pdf
.. [#ref_source2] Evolution of the Internet QoS and Support for Soft Real-Time Applications
