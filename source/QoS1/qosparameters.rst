.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Quality of service parameters
-----------------------------
.. sectionauthor:: Sebastien Duval & Benoît Duhoux

In this section, we will explain the main parameters of QoS.

The parameters described in this document all have affects on QoS. This means that to improve QoS, all these parameters must be taken into account as a whole. They depend on one another. One can not change a single parameter without checking the others.

Throughput
`````````` 
Throughput is the number of data units which can be sent during one time unit. The basic unit of measurement is the bits/second.
Its metric is the bandwidth.
Throughput ensures a level of quality regarding the bandwidth. If you exceed the bandwidth, the excedents packets will be dropped and will compromise the guaranteed quality of the bandwidth on the entire e2e path.

We can try to regulate the throughput using several techniques: 

* One solution is to set the bandwidth via *constant bit rate*. For example, you could limit the bandwidth of each interface to a certain percentage (e.g.: 25% for each interface, if we have 4 interfaces).

* A second solution, more dynamic, is based on the *leaky bucket*. The *leaky bucket* is based on two parameters: the rate *r* defines the output rate of the bucket and the burst size *b* defines the maximum number of packets allowed. Once we exceed *b*, the bucket drops packets. The *leaky bucket*'s algorithm is similar to the *token bucket*'s one described in `Policing and shaping <tokenbucket.html>`_. However, *leaky bucket* drops packets and regularly sends packets based on an average rate. 

Being able to regulate the throughput (as well as other parameters such as delay, jitter and loss) can be useful in areas of interactive applications [#appl_inter] _.
The throughput can also be used in real-time applications with Service-level assurances. Applications such as HDTV or video-conferencing require greater bandwidth. For example, in streaming or real-time video, the throughput is an important factor if we want to maintain a certain level of quality. Having a low throughput is risking to have a poor quality.

Delay
`````
Also known as latency, delay is the time between sending the data from the source and the arrival of these data to the destination.
For example, in the case of voice conversation, this delay is defined as the amount of time it takes for the sound to leave the speaker's mouth and to be heard by the listener.
Generally, we speak of delay between two endhosts. Delay is an intrinsic property of Qos. Indeed, there is always delay when two endhosts are exchanging information.
Each endhost uses the OSI model, that is, the different layers (Application, Transport, Network, Datalink and Physical). Thus, the different layers may have different delays.

This parameter is expressed using the maximum delay bound : :math:`D_{max}`.
The measurement of unidirectional delay might not be exact due to clock synchronization problems. When measuring bidirectional delay - RTT (Round-Trip delay Time) - another problem arises. Indeed, since the paths used for sending and receiving are not always the same, the measurement is dependent of the path taken and thus can never be exact.

Applications having stong requirements on delay are usually the ones using VoIP services in a interactive way. However, tolerant applications (e.g. VoD) can accept some amount of delay.

Components of packets delay
'''''''''''''''''''''''''''
Several components of delay packets exist as in the figure below [#img_src]_.

.. image:: pictures/component_packet_delay.png
	:align: center
	:alt: components of packets delay

In this figure, we find the following delays :

* Network access delay is the delay used to access and cross the local network.
* Serialization delay is the delay used to transmit data from the processor to the interface.
* Propagation delay is the delay used to cross the path between two network nodes.
* Queuing delay is the time spent in priorities queues.

This delay can vary according to the available resources (RAM, bandwidth, ...). The more resources are occupied (resp available), the greater (resp lower) the delay will be. So it can be concluded that the delay is a difficult parameter to predict.

Jitter
``````
The jitter is the variation of the delay in the packet reception. On the sender side, the packets are sent as a stream. If any issues occur in the network such as congestion, improper queueing, etc , then the delay between the reception of the packets may differ.
The jitter is especially important for the interactivity as well as the bandwidth, delay and the packet loss. The concept of interactivity suggests that the different stakeholders do not have to wait too long before receiving the informations. For example, the jitter in a voice conversation is acceptable up to 75 ms. Beyond this measure, it is difficult to adapt the quality of the voice conversation to maintain a good interactivity.

Jitter can be controlled by a jitter buffer. Then the jitter depends on the size of the buffer. The more available is the buffer, the more the jitter will be reduced. However such a buffer is to be used cautiously:

- If the buffer is too large, this may increase the end-to-end delay.
- If the buffer is too small, it can end up with buffer overflow. This can occur when the buffer is already filled and an additional packet comes in. The latter will then be dropped thereby creating a deterioration of the quality of the voice conversation.

The best practice to determine the size of the buffer is to count the packets that arrive late and on this basis, create a ratio over the packets that were correclty processed. Subsequently, this ratio can be used to fit the buffer size to target a specific loss ratio. This can be a good compromise between the jitter and the delay.

Example of jitter buffers can be seen when using video or audio streaming websites. In this case the buffers counter the jitter introduced by the internet so that the playout of the audio/video ressources are still possible. Of course this buffer can cause a delay but it is needed in case of jitter sensitive applications.

Jitter can be computed in several different ways. By definition (delay variation), we have the following formula:

.. math:: J_i = D_{i+1} - D_i

Where  :math:`J_i` is the jitter, :math:`D_{i+1}` the delay of :math:`(i+1)^{th}` data unit and :math:`D_i` the delay of :math:`i^{th}` data unit. The formula shows the difference between these 2 delays that are :math:`D_{i+1}` and :math:`D_1`.

In RTP (Real-time Protocol - protocol used to manage the real-time transmission of mutlimedia data), the jitter can be guessed differently as defined in the `RFC 3550 <http://tools.ietf.org/html/rfc3550>`_ : 

.. math:: J_i = J_{i-1} + \frac{( |D(i-1,i)| - J_{i-1} )}{16}

Where 

- :math:`J_i` is the jitter, :math:`j_{i-1}` the jitter of :math:`(i-1)^{th}` data unit,
- D(i-1,i) is the computation of the difference of relative transit times for the two packets such that :math:`D(i-1,i) = (R_i - R_{i-1}) - (S_i - S_{i-1}) = (R_i - S_i) - (R_{i-1} - S_{i-1})`  where :math:`S_i` is the timestamp of the sender and :math:`R_i` est the arrival time of the packet *i*.  

The difference between D(i-1,i) and :math:`j_{i-1}` is divided by 16 which is equal to the noise reduction ratio. 
It is finnaly added to the value of the previous jitter to get the current value of the jitter.

Both computations summarize the main definitions regarding the quantification of the jitter.

Loss
````
The loss is the percentage of packets that do not arrive to the destination in a specific time interval. This loss can occur through the IP and wirelesss network which can not ensure the delivery of packets. Such packet loss may be caused in different ways, either by a buffer overflow (the packets are dropped) or through a signal degradation, corrupted packets, etc..
If packets are lost, the transport layer will set a retransmission. However this retransmission does not affect the value of the loss, it is simply a losses recovery method.

These packet losses can be significantly influenced by `queueing <bufferacceptance.html>`_ and `scheduling <schedulers.html>`_  techniques.

The loss of packet is one big deal in real-time applications. For example, they can be very harmfull in voice conversations.

This parameter is usually represented by a loss probability. The majority of techniques are based on the active network monitoring. It involves the injection of probe packets in the network being studied. The purpose is to determine how many of them arrives to their destinatio. These probes packets are packets used in an active measurement experiment to collect knowledge on a network (eg Ping sends ICMP probe packets). Using these probes packets will thus measure the overall loss in traffic. This technique is therefore no able to measure the packet losses for the traffic of an individual application.

To fill that gap, there exist also another method called real-time end-to-end packet loss measurement. Such an approach allows for more accurate readings of packet losses for an user or for an application.

Reliability
```````````
The reliability parameter means good delivery of the data to their destination. This notion is not to be confused with the packet loss. Of course these two notions are interconnected. A poor reliability is the consequence of packet loss. To illustrate the difference between these two parameters, we can say that a reliable protocol will use the retransmission to recover from losses.

This parameter is measured taking into consideration the time for a network to fail (or in other words how long will the network be functionnal before it stops). Thus errors, dupplications, etc., are examples of reliabilities gaps. 

Regarding the different ways to quantify the reliability in some systems, various techniques may be used: 

 - Mean Time To Failure (MTTF) : metric often used for systems. This is the time between the placement of a system and its failure (i.e network cable have a lifetime varying from 3 to 100 years). To properly compute the MTBF, it is best to do a time monitoring. That is do a monitoring all along the lifetime of the system. However, for some systems, such technique is rarely relevant if they can hold a hundred years. The MTBF can then be seen as the elapsed time. This implies that the system is not constantly but only used during a periodic rate (duty cycle: percentage of time during which the system is operational). We then obtain the following formula:

	.. math:: \delta = \frac{OT}{MTTF}

	Where :math:`\delta` is the duty cycle and *OT* is the operational time 

- Failure rate: the failure frequency per unit time. The more a system or network is large and complex, the more the failure frequency grows. A failure does not necessarily mean that the system/network stopped working but also that it can generate unsatisfactory performance.
- Mean Time To Recovery (MTTR): time required to repair, restore the components that have stopped working (diagniostic, repair, replacement, etc.). The simplest method is to sum the different times required to repair.
- Mean Time Before Failure (MTBF): This is the time during which the system will operate until the next failure. It can be computed as follows: the number of failure (*F*) all along its commissioning:
  
	.. math:: MTBF = \frac{1}{F}  

	For example, let's combine this with the MTTF. If a system has a MTTF of 100 years and it has already experienced 3 failure, then the MBTF will be of :math:`\frac{100}{3} = 33` ans.

 So the higher the MTBF of a system is, the more the system is reliable. Various alternative exist to calculate the MTBF but that is not the purpose of this chapter to detail them all.

Links
`````

.. rubric:: References

General for all parameters:

- http://www.hh.se/download/18.70cf2e49129168da0158000145746/1341267674792/el-gendi-internet-qos.pdf
- http://www.eolss.net/sample-chapters/c05/E6-108-14-00.pdf
- http://www.ciscopress.com/articles/article.asp?p=357102

Throughput:

- https://sc1.checkpoint.com/documents/R77/CP_R77_QoS_WebAdminGuide/14869.htm
- http://fr.slideshare.net/vimal25792/leaky-bucket-tocken-buckettraffic-shaping
- http://fr.slideshare.net/UmeshGupta3/leaky-bucket-algorithm
- http://www.google.be/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0CCIQFjAA&url=http%3A%2F%2Fclass.ece.iastate.edu%2Fcpre458%2Fcpre558.F00%2Fnotes%2Frt-wan3.ppt&ei=zukwVe_wOMziatrugIAJ&usg=AFQjCNFi19rhSQCD_Y6y7eDbH1R2oqTbhA&bvm=bv.91071109,d.ZWU&cad=rja

Delay:

- http://users.jyu.fi/~timoh/kurssit/verkot/lectures3_4.pdf

Jitter:

- http://www.ciscopress.com/articles/article.asp?p=357102
- http://www.ciscopress.com/articles/article.asp?p=606583&seqNum=2
- http://www.cisco.com/c/en/us/support/docs/availability/high-availability/24121-saa.html
- http://www.ietf.org/rfc/rfc3550.txt?number=3550
  
Loss:

- http://cs.ucsb.edu/~kapravel/publications/packetloss.tma09.pdf
  
Reliability

.. https://books.google.be/books?id=LecC2BhPPxMC&pg=PA37&lpg=PA37&dq=network+MTTF&source=bl&ots=e1sk1zDD86&sig=mAVvuks0WO3UyqnJ_YgM1moGwYs&hl=fr&sa=X&ei=oQ8xVemYNcviaN2ygFA&ved=0CB8Q6AEwAA#v=onepage&q=network%20MTTF&f=false

- Mission-critical Network Planning From Matthew Liotine

.. rubric:: Footnotes

.. [#appl_inter] An interactive application is a human-human application or a human-machine application that requires several interactions and informations transfert between the endpoint of the application. The interactions may be only between the equipments. This applications ca be illustrate with the following examples: monitoring, automated control, etc.
.. [#img_src] This picture has been taken from the QOS presentation slide in the LINGI2142 given by professor O. Bonaventure at the UCL.
