.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

Packet schedulers
-----------------
.. sectionauthor:: Feuillen Gauthier

First in first out (*FIFO*)
```````````````````````````
This is the simplest scheduling mechanism. Packets are processed in the order they are received.

Although this technique is not suitable for providing service differentiation and QoS guarantees, it will be used inside other scheduling mechanisms further in this document.

Priority Scheduling
```````````````````  
Explanation:
''''''''''''
Priority scheduling is based on the following ideas : 

- There are multiple *FIFO* queues :math:`q_1, q_2, ... q_k`.
- Each queue is assigned a priority parameter :math:`p_1, p_2, ... p_k`.
- The queue are served in the order of their priority (of course the highest priority is served first and so on).

The priority scheduling can be preemptive or non-preemptive. Preemptive means that the packet from a lower-priority already in transmission are delayed or dropped if a high priority packet arrives. In almost every cases, it is non-preemptive.

If we consider the non-preemptive version and that the queues :math:`q_1, q_2, ... q_k` are assigned parameters so that :math:`p_1<p_2<...<p_k` then the pseudo code could look like.
::
	while(1){
		q = empty;
		while(q is empty){
			for (i=1; i<=k; i++){
				if !(q_i is empty)
					q=q_i;
			}
		}
		p = q.pop();
		p is transmitted on ouput link;
	}
In this pseudo code, one might ask himself : why not simply go from the highest priority queue (:math:`q_k`) to the lowest one (:math:`q_1`) and then break when a queue is not empty ? This might look like this :
::
	for (i=k; i>0; i--){
		if !(q_i is empty){
			q=q_i;
			break;
 		}
	}
The reason for this is simple, consider that we have 3 queues (:math:`q_1, q_2, q_3`). We are in the inner *while* loop and currently looking if :math:`q_2` is not empty and two packets arrive, the first one goes in :math:`q_3` and the second in :math:`q_1`. Then we check if :math:`q_1` is not empty and we break because it is not. Finally we end up serving :math:`q_1` instead of :math:`q_3`. This problem can not happen with the first solution.

Performances:
'''''''''''''

Let's consider the case of a scheduler with :math:`k` priorities where :math:`p_1<p_2<...<p_k`. If we also consider a uniform packet size of :math:`B` bits and a link capacity of :math:`C` bits/s, then the delay :math:`D_i` experienced by a packet of priority :math:`p_i` is given by:

.. math:: D_i = \frac{B}{C} * \sum_{j=i+1}^{j=k} n_j
	
Where :math:`n_j` is the number of packets in priority queue number j. 

If it is not straightforward for you to understand, this equation can easily be decomposed as follows :

- :math:`\frac{B}{C}` is the time, in second, to transmit a packet of size B with the link of capacity C.
- :math:`\sum_{j=i+1}^{j=k} n_j` is the number of packets to be transmitted in all queues having a priority higher than i.

Since we assumed that the packets were of fixed size, the multiplication of these two components give us the delay for the first packet of the queue with priority :math:`p_i` to be transmitted.

Advantages:
'''''''''''
Priority queuing can be implemented easily since it requires maintenance of only a small number of states per queue.

Drawbacks:
''''''''''
The main drawback of this technique is the possible starvation of lower priority queues. To describe this formally, we consider the queues :math:`q_1,q_2,...,q_k` that are resrespectively supplied at rates :math:`s_1,s_2,...,s_k` and priorities :math:`p_1<p_2<...<p_k`. If there exists :math:`l>1` such that 

.. math:: \sum_{j=l}^{j=k} s_j \geq C
	
Then all queues with an index lower than :math:`l` are suffering of starvation. This also means that if we know, a priori, the supply rates (or an upper bound of it), we can avoid starvation. This is sometimes the case. Indeed, in a univesity campus, the number of internal phones is limited as well as the traffic they generate. Since a phone call requires a low end to end delay, we could put the 'phone traffic' in a higher priority queue and still avoid starvation.

Round-Robin (*RR*)
``````````````````
Explanation:
''''''''''''
Round-Robin scheduling is based on the idea that each queue is assigned a time slice in a circular order. For the ease of the following subsections, we will represent these time slices as a FIFO that is initialized with the initial order (i.e. :math:`[q_1,q_2,...,q_k]`). Once we pop the first element of it (leftmost), we process a packet from this queue and then put the queue number back in the FIFO.

..This is straightforward to understand if we take a look at figure ...

.. todo Figure with example + quick explanation

Round-Robin scheduling is nonpreemptive.
::
	i = 1;
	while(1){
		i = i modulo k
		p = q_i.pop()
		p is transmitted on ouput link;
		i = i + 1	
	}
Performances:
'''''''''''''
If, as we did it for priority scheduling, we consider a uniform packet size of :math:`B` bits and a link capacity of :math:`C` bits/s, then the delay :math:`D_i` experienced by a packet in queue :math:`q_i` is given by : 

.. math:: D_i = \frac{B}{C} * next(FIFO, q_i)

Where :math:`next(FIFO, q_i)` represents the index (counting from the left and starting at 0) of the next appearance of :math:`q_i` in the FIFO. For example if the :math:`FIFO` looks like :math:`[q_2,q_3,q_1]` then :math:`next(FIFO, q_1)` returns :math:`2`. Indeed, in such an example, :math:`q_1` will have to wait that :math:`2` packets are transmitted before being served.

Advantages & Drawbacks:
'''''''''''''''''''''''
A first advantage of the Round-Robin scheme is its ease of implemention, we did it with a FIFO but there exists other ways e.g. simple linked list,... This ease of implementation is mostly due to the fact that we do not need any priority, only a schedule to follow. However not having priorities also means that we are not able to differentiate flows.
 
A second advantage is that if we consider that all the packet are of the same size, this scheme is fair at queue level. Nevertheless, if this assumption turns out to be wrong, then the above formula and the fairness both go away. Indeed if a queue has only one time slice but processes packet 10 times bigger,the fairness is not respected and the formula can not be used anymore.

Deficit Round-Robin (*DRR*)
```````````````````````````
Explanation:
''''''''''''
Deficit Round-Robin is based on the same idea that Round-Robin but each queue :math:`q_i` has an associated counter :math:`d_i`. Each time :math:`q_i` is visited, :math:`d_i` is incremented by a quantum :math:`q`. Once a queue has accumulated enough quantum to send a packet, the packet is sent and the counter is decremented by the size of the packet. This can be represented with the following algorithm :
::
	i = 1;
	while(1){
		i = i modulo k
		d_i = d_i + q %Remember q is the quantum here
		if (first_packet of q_i larger than d_i){
			packet stays in q_i;
		}else{
			packet is transmitted on output link;	
			d_i=d_i- packet length;	
			if (q_i is empty){
				d_i=0;
			}	
		}
		i = i + 1;
	}

The last condition ":math:`q_i` is empty" is important so that a queue doesn't accumulate unnecessary quantum.

Advantages & Drawbacks:
'''''''''''''''''''''''
The main advantage of deficit Round-Robin compared to a simple Round-Robin is that it is always fair a queue level regardless of the packet size. Indeed the use of an external measurement unit (the quantum) is independant of the packet size. Yet it also requires to maintain more variables for each queue and the implementation is more difficult.

As for the simple Round-Robin, we can not make any difference between the flows, therefore it is not suitable for QoS requirements.

Weighted Round-Robin (*WRR*)
````````````````````````````
Explanation:
''''''''''''
In this version of the Round-Robin we make a simple change : we had to each queue :math:`q_i` a weight :math:`w_i` and queue numbers can appear several times in the FIFO. The weight represents the importance of the queue, this importance is reflected by the fact that the greater the weight, the more times the queue number will be present in the FIFO.

For example, if we have two queues :math:`q_1` and :math:`q_2` with their associated weights :math:`w_1 = 2` and :math:`w_2=1` then the FIFO could look like :math:`[q_1,q_2,q_1]`. In this FIFO :math:`q_1` appears 2 twice as much as :math:`q_2` because its weight is twice as important as the one of :math:`q_2`. 

One might ask himself how to create the FIFO. It is quiet simple to do and easy to understand with an example. 

Let's assume that we have :math:`3` queues respectively :math:`q_1,q_2,q_3` with their weights :math:`w_1 =1`, :math:`w_2=2` and :math:`w_3=3`. We create a FIFO with a length of :math:`1+2+3=6`. 

Let's fill it, begining with the queue demanding the most time slices, that is :math:`q_3`. :math:`q_3` must appear 3 times and in order to respect fairness, the time slices should be spaced as much as possible. This gives us the following FIFO :math:`[q_3,\epsilon,q_3,\epsilon,q_3,\epsilon]` where :math:`\epsilon` means empty. Then let's get our second queue :math:`q_2`, repeating the same operation gives us :math:`[q_3,q_2,q_3,q_2,q_3,\epsilon]`. Finally adding :math:`q_1` produces :math:`[q_3,q_2,q_3,q_2,q_3,q_1]`


The pseudo code for the weighted Round-Robin scheme would look like :
::
	i = 1;
	FIFO =  new FIFO(2,3,1,2,3,...) %We create a new FIFO initialized as we want it.
	while(1){
		i = FIFO.pop()
		p = q_i.pop()
		p is transmitted on ouput link;
		FIFO.push(i);
	}


Performances:
'''''''''''''
The performance of this scheme is exactly the same as the one of a simple Round-Robin.

Advantages & Drawbacks:
'''''''''''''''''''''''
The main advantage compared to a simple Round-Robin is that we can prioritize some queue over another according to their weight. But as for the simple Round-Robin the fairness is dependant on the veracity of the fixed size assumption.

In addition, as we can see it with the pseudo code, it is quite easy to implement.

References:
```````````

http://www.hh.se/download/18.70cf2e49129168da0158000145746/1341267674792/el-gendi-internet-qos.pdf

Questions - Priority Scheduling:
````````````````````````````````

For the following questions consider the following : 

A scheduler using non-preemptive priority scheduling. There are 3 queues : :math:`q_1, q_2, q_3` and priorities are as follows:
        
        * :math:`p_1 = 3`
        * :math:`p_2 = 2`
        * :math:`p_3 = 1`
        
Consider an output link with a capacity of 100%.
 
 
Question 1:
'''''''''''
 
.. question::
	:nb_pos: 1
	:nb_prop: 3

	Consider the following input traffic (:math:`in_i` means the traffic input of queue :math:`q_i`). : 

		* :math:`in_1 = 60\%`
		* :math:`in_2 = 30\%`
		* :math:`in_3 = 30\%`

	What is the actual bandwith of the output link used by each queue (:math:`out_i` is the bandwith of queue :math:`q_i`) ?
	
	.. positive::
		:math:`out_1 = 60\%`
		, :math:`out_2 = 30\%`
		and :math:`out_3 = 10\%`

	.. negative::
		:math:`out_1 = 60\%`
		, :math:`out_2 = 10\%`
		and :math:`out_3 = 30\%`
		
	.. negative::
		:math:`out_1 = 40\%`
		, :math:`out_2 = 30\%`
		and :math:`out_3 = 30\%`

Question 2:
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	This question is a bit more complex. Consider the input traffic :
	
		* At :math:`t_0` :math:`in_1 = 110\%`, :math:`in_2 = 30\%` and :math:`in_3 = 30\%`
		* At :math:`t_1` :math:`in_1 = 0\%`, :math:`in_2 = 30\%` and :math:`in_3 = 30\%`
		* At :math:`t_2` :math:`in_1 = 0\%`, :math:`in_2 = 0\%` and :math:`in_3 = 0\%`
		
	Where :math:`\Delta t = t_1-t_0 = t_2-t_1`. What is time :math:`t` needed to deal with all the traffic, considering that queues have an unlimited buffer to store data that can't be sent immediately.
	
	.. negative::
		:math:`\Delta t < t < 2*\Delta t`

	.. negative::
		:math:`t = 2*\Delta t`
		
	.. positive::
		:math:`2*\Delta t < t`

Questions - Simple Round-Robin:
```````````````````````````````

For the following questions consider the following : 

A scheduler using Round Robin scheduling. There are 3 queues : :math:`q_1, q_2, q_3` that are served in the following order: :math:`q_1 \rightarrow q_2 \rightarrow q_3`. Again, consider an output link with a capacity of 100%.
	
Question 1:
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	Under which assumption are all the queues served equally ?
	
	.. negative::
		They all have the same input of traffic.

	.. negative::
		They all have the same size.
		
	.. positive::
		All the packets have the same size.

Question 2:
'''''''''''
		
.. question::
	:nb_pos: 1
	:nb_prop: 3

	Consider the following input traffic (:math:`in_i` means the traffic input of queue :math:`q_i`) and uniform packet sizes:
	
		* :math:`in_1 = 50\%`
		* :math:`in_2 = 20\%`
		* :math:`in_3 = 60\%`
		
	What is the actual bandwith of the output link used by each queue (:math:`out_i` is the bandwith of queue :math:`q_i`) ?
	
	.. negative::
		:math:`out_1 = 50\%`
		, :math:`out_2 = 20\%`
		and :math:`out_3 = 30\%`

	.. negative::
		:math:`out_1 = 33.33\%`
		, :math:`out_2 = 20\%`
		and :math:`out_3 = 33.33\%`
		
	.. positive::
		:math:`out_1 = 40\%`
		, :math:`out_2 = 20\%`
		and :math:`out_3 = 40\%`

Question 3:
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	Consider the following input traffic (:math:`in_i` means the traffic input of queue :math:`q_i`) and packet sizes (:math:`s_i` corresponds to the packet size of queue :math:`q_i`):
	
		* :math:`in_1 = 33.33\%`, :math:`s_1 = 1`
		* :math:`in_2 = 33.33\%`, :math:`s_2 = 2`
		* :math:`in_3 = 33.33\%`, :math:`s_3 = 4`
		
	What is the actual bandwith of the output link used by each queue (:math:`out_i` is the bandwith of queue :math:`q_i`) ?
	
	.. negative::
		:math:`out_1 = 14\%`
		, :math:`out_2 = 29\%`
		and :math:`out_3 = 57\%`

	.. negative::
		:math:`out_1 = 33.33\%`
		, :math:`out_2 = 33.33\%`
		and :math:`out_3 = 33.33\%`
		
	.. positive::
		:math:`out_1 = 19\%`
		, :math:`out_2 = 24\%`
		and :math:`out_3 = 57\%`
		
Questions - Deficit Round-Robin:
````````````````````````````````
For the following question consider a scheduler using Deficit Round-Robin scheduling. 

Question 1:
'''''''''''
.. question::
	:nb_pos: 2
	:nb_prop: 3

	Select the correct(s) affirmation.
	
	.. positive::
		The fairness of *DRR* doesn't depends on the size of the packets.

	.. negative::
		The use of a quantum :math`q` makes this scheme easier to implement than a simple *RR*.
		
	.. positive::
		This scheme is not suitable for QOS requirements.

Questions - Weighted Round-Robin:
`````````````````````````````````
For the following question consider a scheduler using Weighted Round-Robin scheduling. There are three queues served in the following order : :math:`q_1 \rightarrow q_2 \rightarrow q_1 \rightarrow q_3 \rightarrow q_1 \rightarrow q_2`

Question 1:
'''''''''''
.. question::
	:nb_pos: 1
	:nb_prop: 3

	Select the correct(s) affirmation.
	
	.. negative::
		This scheme is suitable for QOS requirements in addition to always be fair.

	.. negative::
		The performances of this scheme in term of delay is better than for simple *RR*.
		
	.. positive::
		This scheme is suitable for QOS requirements.
				
Question 2:
'''''''''''
.. question::
	:nb_pos: 1
	:nb_prop: 3

	Consider the following input traffic (:math:`in_i` means the traffic input of queue :math:`q_i`) and uniform packet sizes:
	
		* :math:`in_1 = 50\%`
		* :math:`in_2 = 20\%`
		* :math:`in_3 = 60\%`
		
	What is the actual bandwith of the output link used by each queue (:math:`out_i` is the bandwith of queue :math:`q_i`) ?
	
	.. positive::
		:math:`out_1 = 50\%`
		, :math:`out_2 = 33\%`
		and :math:`out_3 = 17\%`

	.. negative::
		:math:`out_1 = 33.33\%`
		, :math:`out_2 = 33.33\%`
		and :math:`out_3 = 33.33\%`
		
	.. positive::
		:math:`out_1 = 40\%`
		, :math:`out_2 = 20\%`
		and :math:`out_3 = 40\%`



Questions - Combining schedulers:
````````````````````````````````` 

We have 4 queues : :math:`q_1`, :math:`q_2`, :math:`q_3` and :math:`q_4` with the following priorities : :math:`p_1 = 2`, :math:`p_2 = p_3 = p_4 = 1`. Since :math:`q_2`, :math:`q_3` and :math:`q_4` have the same priority, we decide to use a WRR scheduler. In this scheduler, :math:`q_2` appears 66 times, :math:`q_3` appears 33 times and :math:`q_4` appears once.

Given that the input of each queue is : 

	* :math:`in_1 = 10\%`
	* :math:`in_2 = 30\%`
	* :math:`in_3 = 60\%`
	* :math:`in_3 = 50\%`



Question 1:
'''''''''''
.. question::
	:nb_pos: 1
	:nb_prop: 3

	What is the actual bandwith of the output link used by each queue (:math:`out_i` is the bandwith of queue :math:`q_i`) ?
	
	.. positive::
		:math:`out_1 = 10\%`
		, :math:`out_2 = 30\%`
		, :math:`out_3 = 33\%`
		and :math:`out_4 = 27\%`

	.. negative::
		:math:`out_1 = 10\%`
		, :math:`out_2 = 59.4\%`
		, :math:`out_3 = 29\%`
		and :math:`out_4 = 0.9\%`
		
	.. negative::
		:math:`out_1 = 25\%`
		, :math:`out_2 = 25\%`
		, :math:`out_3 = 25\%`
		and :math:`out_4 = 25\%`	
		
Question 2:
'''''''''''

.. question::
	:nb_pos: 1
	:nb_prop: 3

	What is the maximum bandwith that :math:`q_2` can achieve if :math:`in_1 = 20\%`?
	
	.. positive::
		:math:`max(out_2) = 52.8\%`

	.. negative::
		:math:`max(out_2) = 66\%`
		
	.. negative::
		It depends on :math:`in_2`
	



	
