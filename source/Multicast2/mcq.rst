.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. sectionauthor:: Alexandre D'Hondt, Henri Crombe, Hussein Bahmad, Julie Chatelain, Mallory Declercq

Multiple Choice Questions
=========================

**Multicast Routing**

1. Select the wrong affirmation. The multicast ...

 a. Is suitable for distributed applications.
 b. Eliminates traffic redundancy.
 c. Increases the CPU load of routers.
 d. Operates at the network layer.

 Answer: c.


**Ethernet Multicast**

1. Which one(s) of the following Ethernet Multicast address is correct and could be used in a LAN :

 a. 01-00-5E-DE-82-00
 b. 00-00-5E-DE-82-00
 c. 00-00-6F-DE-82-00
 d. 01-00-5E-DE-82-AF
 e. 01-00-5F-FF-FF-FF
 f. 00-00-5E-FF-FF-FF

 Hint : Internet multicast applications use the range 01-00-5E-00-00-00 to 01-00-5E-FF-FF-FF

 Answer: a. d.

2. An Ethernet multicast address must be assigned to an existing host for a sender to be able to send traffic on this address :

 a. True 
 b. False

 Answer: b.


**IP Multicast**

1. IPv4 multicast adresses belgongs to :
 a. IP class A
 b. IP class B
 c. IP class C
 d. IP class D

 Answer: d.

2. A host just needs to advertise the multicast address to its local router to receives the traffic intended to that address.

 a. True
 b. False

 Answer: a.

3. In order to achieve the translation between a Layer 3 IP multicast address and Layer 2 multicast MAC address :

 a. The low order 23 bits of the IP multicast address are mapped directly to the low order 23 bits in the MAC-layer multicast address.
 b. The high order 23 bits of the IP multicast address are mapped directly to the low order 23 bits in the MAC-layer multicast address.
 c. The low order 23 bits of MAC-layer multicast are mapped directly to the low order 23 bits in of the IP multicast address.
 d. The low order 23 bits of MAC-layer multicast are mapped directly to the high order 23 bits in of the IP multicast address.

 Answer: a.

4. The field Address type in an IPv6 address is used to indicate the nature of certain multicast addresses.

 a. True 
 b. False

 Answer: b.


**Internet Group Management Protocol**

1. Which field is not included in the IGMP message?

 a. Type
 b. Maximum response time
 c. Group address
 d. TTL

 Answer: d.


**Flood and Prune Protocol**

1. Select the wrong affirmation. Flood and prune protocols ...

 a. Are used to react in case of failure.
 b. Are used to construct shortest path tree.
 c. Are also called `Reverse path broadcast`.
 d. Requires the routers to store a prune state.

 Answer: a.


**Multicast OSPF**

1. Select the right affirmation :

 a. MOSPF is suitable for networks with large numbers of multicast source-group.
 b. MOSPF is a totaly new version of OSPF built from scratch.
 c. MOSPF adds a new kind of link-state advertissement called group-membership-LSA.
 d. MOSPF doesn't require routers to have a complete topology of the network.

 Answer: c.

2. Which kind of routing protocol is used with MOSFP ?

 a. link-state routing
 b. distance-vector routing

 Answer: a.

3. What is the role of the group-membership-LSA ?

 a. provide the means of distributing membership information throughout the MOSPF routing domain.
 b. provide a dynamic mechanism to slow down link-state advertisement updates.
 c. discribe the neighbors of a router and its interfaces.
 d. Summarize information about one of the areas of the network.

 Answer: a.


**Any-Source and Source-Specific Multicast**

1. Select the right affirmation, SSM is well suited for :

 a. One-to-one multicast.
 b. One-to-many multicast.
 c. Many-to-one multicast.
 d. Many-to-many multicast.

 Answer: b.

2. Does ASM support multicast groups with many senders ?

 a. True
 b. False

 Answer: a.


**Core-Based Trees**

1. What is a core router ?

 a. It is the sender (source) in a multicast group.
 b. It is the root of the shared tree.
 c. IT is a leaf of the shared tree.
 d. None of the above.

 Answer: b.

2. When is a branch added to the shared tree ?
	
 a. When the core send a "Join Ack" message.
 b. When a sender starts forwarding data.
 c. When a group member sends a "Join Request" message.
 d. None of the above.

 Answer: a.

3. What represents a branch of the shared tree ?

 a. The shortest path between a group member and the core router.
 b. The shortest path between a sender and a receiver.
 c. The path linking all the members of the group.
 d. All of the above.

 Answer: a.


**Protocol Independent Multicast - Sparse Mode**

1. What does `PIM` means ?

 a. Protocol Information Multicast.
 b. Personal Information Multicast.
 C. Protocol Independent Multicast.
 d. None of the above.

 Answer: c.

2. How can a sender (source) forward data in PIM-SM ?

 a. It sends the packet to the RP using unicast.
 b. It sends the packet directly to the receivers using a source tree.
 c. It sends the packet to the RP using a source tree.
 d. All of the above.

 Answer: d.

3. What is the difference between PIM-SM and CBT ?

 a. They are the same.
 b. With CBT, there's as many core as the number of source.
 c. With PIM-SM, a source can sends packet directly to a receiver.
 d. With CBT, a source sends the packets to the core via unicast.

 Answer: c.

