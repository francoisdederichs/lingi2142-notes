.. sectionauthor:: Julie Chatelain

Core-Based Trees
================

CBT (:rfc:`2189`, :rfc:`2201`) is a multicast routing architecture that builds only one tree per group (unlike others that build one tree per sender). The tree is shared by all group's senders and receivers. Its goal is to make IP Multicast Scalable. [#fcbtdef]_


How does it work ?
------------------

CBT works by constructing a tree of routers with one (or multiple) core(s) :

- A single node (router) is the core (root) of the tree.
- Branches (routers linked together) emerge from the root. 
- Each multicast group has a core address and a globally unique group identifier (group-id).
- The core address is the unicast address of the tree root. Packets are send to the tree using this address.
- The group-id is used to multicast the packets once they are on the tree.

Once on the tree, a packed is multicasted based on a globally unique group identifier (group-id).

.. figure:: imgs/cbt-example.png
   :align: center
   :scale: 80

   Example of CBT


Principles
----------

1. A single node (router) is the core (root) of the trees.
2. One single tree per group.
3. The receivers are the leafs of the tree.
4. The branches of the tree represent the shortest path between leaf and core (root).

CBT is bidirectional : It uses the `Reverse Path Forwarding`. Receivers (leafs) send and receive packets via the branch of the tree. Therefore, the same path is used to send and receive message and packets. 


Step by step
------------

- **Initialization**

 0. The core routers are statically configured.

- **Tree Formation**

CHANGE :  no tabular for figure

.. figure:: imgs/cbt-tree-formation.png  
	:scale: 80                            
	:align: center                       
   
.. figure:: imgs/cbt-tree-formation2.png 
	:scale: 80                       
	:align: center                   
..

 1. A group member appears on the network.
 2. Its local CBT router finds the group core address by looking up the multicast address.
 3. The router sends a `Join Request` towards the core.
 4. The join request is forwarded hop-by-hop using the unicast forwarding table.
 5. Along the way the request message creates a temporary state (entry) in all CBT routers.

  a) Typical entry contains :

   - group-id
   - parent address (next upstream router)
   - number of children (downstream routers)
   - which interfaces the parent and children are connected to.

  b) The router is in a state of pending membership.

 6. The join request is forwarded until it reaches the tree (either the root or the middle of a branch).

.. image:: imgs/cbt-tree-formation-new.png
   :align: center
   :scale: 80

..

 7. The core confirms the join request by sending a `Join Ack` message.
 8. The ack message travels the same way as the request message in the reverse direction.
 9. Along the way the ack message makes the temporary state permanent in the routers and a new branch is grown.

- **Data forwarding**

.. image:: imgs/cbt-data-forwarding.png
   :align: center
   :scale: 80

..

 1. Multicast packets are send to the multicast tree using unicast routing. (That way, multicast groups and packets are invisible to routers not on the tree. These routers don't need to keep information about the groups).

  a) The group core address is put in the destination field and the group-id in the option field of the IP packet's header.
  b) Once on the tree, the core address in the destination field is replaced by the group-id.

 2. The multicast packets are spread along the tree according to the group-id.


- **Quitting**

CHANGE : SAME
.. figure:: imgs/cbt-quit.png
	:scale: 80
	:align: center

.. figure:: imgs/cbt-quit2.png
	:scale: 80
	:align: center

..

 1. A receiver wants to quit a multicast group.
 2. The router sends a `Quit Request` message upstream.
 3. The quit request is acknowledged with a `Quit Ack` by the upstream (parent) router.
 4. If the parent router doesn't have any more child belonging to the group, it also sends a `Quit Request` upstream.

- **In Case of Failure**

 1. The downstream routers check the activity of their parent by periodically sending `Echo Request` message towards them.
 2. The parent confirms with a `Echo Reply` message.
 3. A failure is detected when the parent doesn't reply.
 4. Two options are possible:

  a) the router hides the failure from the rest of the branch and submits a new `Join Request` upstream.
  b) the router informs the downstream routers of the failure (by sending a `Flush-Tree` message) and let them deal with it. Each downstream router can individually try to re-attach itself to the tree.


Advantages and drawbacks
------------------------

**Advantages**

 1. `Scalability` : since there's only one tree per group the amount of state that needs to be stored at each router is equal to the number of groups. Routers not on the tree require no knowledge of the tree.
 2. Only routers on a future branch are involved in the tree creation.
 3. CBT takes advantages of the underlying unicast algorithm but is separated from it.

**Drawbacks**

 1. `Core Placement` : the lenght of the path between members of the group depends on the position of the core. The core can't always be placed at the best position for each member. So CBT doesn't always provide the shortest paths between members of a group.
 2. `Core Failure`: if the core fails, everything goes down. It can be solved with multiple cores but it increases the complexity.

  a) Single Core with several backup cores : If the primary core fails, the scenario is the same as in the case of path or node failure. The branch routers re-attach themself to another core.
  b) Multiple Core : several small trees attached to different cores. Each core must be link to another and there must be a protocol to handle failure.


.. rubric:: References

.. [#fcbtdef] http://web.archive.org/web/20051201222035/http://www.cse.iitk.ac.in/research/mtech1997/9711105/node15.html, 
              http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.481.8667&rep=rep1&type=pdf, 
              http://www.cse.iitk.ac.in/users/braman/courses/cs625-fall2003/lec-notes/lec-notes19-1.html
