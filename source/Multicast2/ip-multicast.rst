.. sectionauthor:: Henri Crombe

IP Multicast
============

As said previously, in multicast, a sending host can simply sends packets to one multicast address to spread the traffic to the receiving hosts. The multicast address is slightly special because it refers to group of host and does not refer to a single host. Unlike unicast addresses, hosts can dynamically subscribe to multicast addresses and by doing so they receive traffic  belonging to these addresses (i.e. group). 

The IP multicast service model can be summarized like this:

- A single sender or more,  send packet to a multicast address
- Receivers express their interest in a specific multicast address by the mean of IGMP protocol.
- Routers conspire to deliver traffic from the sender(s) to the receiver(s).

Sending multicast traffic is like sending unicast traffic except that the destination address is a multicast address that refers to a group of hosts. However, receiving traffic requires hosts to tell its local router that it is interested in a particular multicast group address.

In order to make this group communication work for large-scale systems, senders and intermediates routers doesn't know who is listening to them. Multicast traffic senders does not know who might receives the traffic.
IP Multicast protocol supports both IPv4 and IPv6.  Both protocols employs group-addressing scheme, multicast routing protocols and a group-membership mechanism that lets hosts and routers signal their intentions to join or leave a multicast group. Nonetheless, there are differences between the two approaches. [#ipv6-multicast]_


**IPv4 multicast model**

- Service :
 As said above, the IGMP protocol is used to extend hosts functionality  (i.e. receive mutlicast traffic). When IPv4 addressing was designed, it was divided into 4 different IP classes :
    A class has IP range from 1.0.0.0 to 126.255.255.255
    B class has IP range from 128.0.0.0 to 191.255.255.255
    C class has IP range from 192.0.0.0 to 223.255.255.255
    D class has IP range from 224.0.0.0 to 239.255.255.255

IP class D is reserved for multicast addresses and that this kind of address that senders and hosts used to refer to a multicast group. 
An IPv4 multicast address is made of 32 bits and always starts with the high-order 4-bit set to 1110 so that the a multicast IPv4 address should always start with a number going from 224 to 239. This means that there are 2^38 possible groups.

To support IP multicast hosts, the host service interface must extended :
A host must must be able to join a group, meaning that it must be able to reprogram its network level (level 3) and consequently the lower layers to receive packets addressed to multicast group addresses.
An application that has joined a multicast group and then sends to that group must be able to select whether it wants to configure its loop-back address to receive its own packets. 
When an application tells the host networking software to join a group, the host software checks if the host is already part of that group. If not, it sends out an IGMP membership report message to its local router. It also maps the IP address to a lower-level address (Ethernet) and reprograms its network interface to accept packets sent to that address. This means that hosts can join on group 'on an interface'. So, hosts which have more than on enetwork card can choose which one will receive multicast packets.

As said above, Multicast IP address live in the 224.0.0.0 - 239.255.255.255 range but what about MAC addresses and Ethernet frames ?  What do we do on layer 2 to make multicast work ?  
In order to achieve the translation between a Layer 3 IP multicast address and Layer 2 multicast MAC address, the low order 23 bits of the IP multicast address are mapped directly to the low order 23 bits in the MAC-layer multicast address. Because the first 4 bits of an IP multicast address are fixed according to the class D convention, there are 5 bits in the IP multicast address that do not map to the MAC-layer multicast address. Therefore, it is possible for a host to receive MAC-layer multicast packets for groups to which it does not belong. However, these packets are dropped by IP once the destination IP address is determined.

Example :

PROBLEME AVEC L'IMAGE -> NON RECONNUE image:: imgs/ip-multicast-address-mapping.png


** IPV4 vs IPv6 multicast service **

To simplify comparison between IPv4 and IPv6 multicast, the table hereafter lists the key IP multicast functions and how IPv4 and IPv6 each handle them. [#ipv6-multicast]_

.. image:: imgs/ip-multicast-comparison.png
   :align: center
   :scale: 70

..

Let's start by looking at multicast under IPv6. Under IPv6, multicast addresses are allocated from the multicast block. This is 1/256th of the address space, consisting of all addresses that begin with `1111 1111`. Thus, any address starting with `FF` in colon hexadecimal notation is an IPv6 multicast address.  The remaining 120 bits of address space are enough to allow the definition of 1.3 trillion trillion trillion of multicast addresses.  

**IPv6 multicast packet**

.. image:: imgs/ip-multicast-ipv6-packet.png
   :align: center
   :scale: 80

..

- Address type : The first eight bits are always ``1111 1111`` to indicate a multicast address.
- Flags : Four bits are reserved for flags that can be used to indicate the nature of certain multicast addresses. 
- Scope : These four bits are used to define the scope of the multicast address. 16 different values from 0 to 15 are possible. This field allows multicast addresses to be created that are global to the entire Internet, or restricted to smaller spheres of influence such as a specific organization. This prevents multicast packets from crossing administrative network boundaries and traversing links and networks in which they don't belong.
- Group ID : indentifies a specific multicast group address defined within the packet's scope

In IPv6, hosts and routers use Multicast Listener Discovery (MLD) protocol to exchange group membership information. The MLD protocol lets routers discover destination multicast hosts on directly attached subnets (that is, Ethernet), as well as their respective multicast group addresses of interest. When the leaf router learns this information, it can pass to the multicast routing protocol (PIM for example). Then the routers in the network then process multicast routing control packets to build the tree required deliver multicast packets from the source to the multiple destinations.
 A host can use MLD to signal its desire to listen to (or remove itself from) a multicast stream identified by a specific group address.


.. rubric:: References

.. [#fmulticast] http://www.firewall.cx/networking-topics/general-networking/107-network-multicast.html

.. [#ipv6-multicast] https://icampus.uclouvain.be/claroline/backends/download.php?url=L1RoZW9yeS85LU11bHRpY2FzdC9pcHY2bXVsdGljYXN0LnBkZg%3D%3D&cidReset=true&cidReq=INGI2142

