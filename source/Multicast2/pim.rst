.. sectionauthor:: Julie Chatelain

Protocol Independent Multicast - Sparse Mode
============================================

PIM-SM is called `Protocol Independent` (:rfc:`4601`) because it is not dependent on any particular unicast routing protocol. It is called `Sparse Mode` because it expects that there will be only a few sparsely distributed group members.


How does it work ?
------------------

It builds unidirectional shared trees rooted at a `Rendez-vous Point` (RP) per group. It optionally creates shortest-path trees per source. It is a protocol created for efficiently routing packets to multicast groups that span wide-area and inter-domain internet. [#fpimdef]_

.. figure:: imgs/pim-example.png
   :align: center
   :scale: 80

   Example of PIM working

..

**Roles of the Rendez-vous Point**

 1. It is the root of the non-source-specific shared trees (multicast distribution trees) for the receivers (similar to the core in CBT).
 2. It is the leaf of the source-specific tree for the senders.
 3. It receives `Join` messages from receivers.
 4. It receives data from the senders and forwards it onto the shared tree.
 5. This is the point where the receivers meet the sources.


Events
------

- When a new receiver wants to join the group

.. image:: imgs/pim-receiver-joining.png
   :align: center
   :scale: 80

..

 1. A local routers becomes the Designated Router (DR).
 2. The DR sends a PIM `Join` message toward the RP for the group.
 3. The Join message is forwarded hop-by-hop towards the RP (root of the tree for the group). 
 4. Along the way the Join message creates a `multicast tree state` [(\*,G) State] in the routers. The routers is added to the tree. The branch is slowly growing.
 5. The Join message is forwarded until it reaches the tree (either the RP or a router that already has a `Join state` for that group). 
 6. Join messages are resent periodically as long as the receiver stays in the group.

- When the receivers want to leave the group

  The DR sends a `Prune` message toward the RP. Even if the `Prune` message is not correctly sent, if the upstream router doesn't receive a Join message for a certain time, the state will time out.

- When another sender starts

.. image:: imgs/pim-receiver-registering.png
   :align: center
   :scale: 80

..

 1. The sender starts sending data destined for a multicast group.
 2. The sender's local router (DR) takes the data packets and unicast-encapsulates (registers) them.
 3. The DR sends the encapsulated packets (PIM `Register` packets) to the RP (via unicast).
 4. The RP receives the PIM `Register` packets and decapsulates them.

  a) If the data sending rate is high, the RP sends a PIM `Join` message toward the sender to build a Source Tree. Along the way, the message creates an (S,G) State in the router and adds them to the Source Tree.
  b) The packets from the sender will start to flow along the Source Tree.
  c) When the RP starts receiving packets via the Source Tree, it sends a `Register-Stop` message back to the sender's DR to make it stop sending encapsulated packets.


.. figure:: imgs/pim-sender-joining.png
	:scale: 80 
	:align: center
..

.. figure:: imgs/pim-sender-stopping.png
	:scale: 80 
	:align: center
..

 5. The RP forwards the packets onto the shared tree.
 6. The packets are spread along the tree by following the multicast tree state ([(\*,G) State]) in the routers. 
 7. The packets reach all the receivers for the multicast group.

.. Note:: if the RP has no receivers for the data send by a source, it still adds the source into the PIM table but then sends a `Register-Stop` message.


Optimality
----------

- Is an RP rooted tree optimal ?

    No. The path from the sender to the RP to the receivers may not be the shortest one.
    To solve this problem, it is possible for the receivers to get content directly from the sender. The Source Tree (tree between sender and RP) just need to grow another branch toward the receiver. For that, the receiver send a (S,G) `Join` message towards the source. Along the way, the message creates a (S,G) State on the routers and adds them to the branch. When the `Join` message reaches the source (or a router that's already on the Source Tree), the branch is grown and the shortest-path tree (SPT) between sender and receiver is finished.


.. image:: imgs/pim-receiver-joining-spt.png
   :align: center
   :scale: 80

..

    The data packets from the source start flowing along the SPT (shortest-path tree). When the receiver starts receiving the pack via the SPT, it will drop packets from the RPT (RP Tree) and sends a (S,G) Prune message towards the RP.


.. image:: imgs/pim-receiver-pruning.png
   :align: center
   :scale: 80

..

    A (S,G) `Prune` message is different from an (*,G) Prune message. It doesn't detach the receiver branch from the RP Tree, it only indicates that it doesn't want packets from the source S. The `Prune` message is forwarded hop-by-hop until it reaches either the RP or a router that still needs the traffic from the source S for another receiver.

.. image:: imgs/pim-receiver-spt.png
   :align: center
   :scale: 80

..


RP Address Distribution
-----------------------

It can be done with a bootstrap mechanism or by static configuration.
With the Bootstrap Router (BSR) mechanism :

 1. One router in each PIM domain is elected the BSR.
 2. All the routers that are RP candidates periodically unicast their candidacy to the BSR.
 3. The BSR picks a RP-set and periodically announces this set in a message.

To map a group to a RP, a router uses a hash function to hash the group address into the RP-set.


Advantages
----------

 1. PIM-SM scales rather well for wide-area usage. Since it uses a shared tree, it uses less memory (only equivalent to the number of group).
 2. Unicast routing protocol-independent.
 3. Since PIM-SM can optimise its trees after formation (by creating a shortest-path tree between sender and receiver), the RP location is less critical than the Core location in CBT. But it uses more memory.
 4. Only routers on a future branch are involved in the tree creation.


.. rubric:: References

.. [#fpimdef] http://en.wikipedia.org/wiki/Protocol_Independent_Multicast
 
.. [#fpimdef] http://www.juniper.net/documentation/en_US/junos13.3/topics/concept/multicast-pim-sparse-characteristics.html

.. [#fpimdef] http://www.cl.cam.ac.uk/~jac22/books/mm/book/node79.html

