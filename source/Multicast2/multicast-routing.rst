.. sectionauthor:: Alexandre D'Hondt

Multicast Routing
=================

Why multicast ?
---------------

The need of multicast was born because some applications require data to be sent from a unique source to multiple destinations. (Multipoint communication)
The examples of such applications include audio and video broadcasts, teleconferencing applications (video/audio), TV distribution, streaming media etc. 

.. figure:: imgs/multicast-routing-casts.png
   :align: center
   :scale: 100

   The three ways to send data [fmulticast]_

The figure above presents three ways to send data from one specific source. In the unicast approach the sender just has to send one copy of data to one specific destination. In a broadcast solution data is sent to all hosts in a given network, and the host just drop the data if he is not interested in it (hosts have to be in the same LAN).

One can directly notice that the multicast approach can be in fact several unicast communications. This approach is quite sufficient if there are few hosts that are members of a certain multicast group. In fact, the sender has to send one copy of the data separately to each host in the multicast group. This option is not viable for a large multicast group because transmitting the same data multiple times consume a lot of resources (i.e. CPU load).

.. figure:: imgs/multicast-routing-unicast-vs-multicast.png
   :align: center
   :scale: 70

   Unicast versus Multicast [funivsmulti]_

In order to make multicast efficient, one needs :

- to apply some `other protocols` and some stuffs at the network layer
- to have a `set of addresses` that represents a multicast group as the destination of the data
- `mechanisms` that allow a given host `to join or leave` a given multicast group are also necessary
- a way to specify `paths` from the sender to the members of multicast group

So a multicast routing protocol allowing to set up paths, also called distribution tree, is needed.

.. figure:: imgs/multicast-routing-delivery.png
   :align: center
   :scale: 100

   Multicast delivery example in an IP network [fmulticast]_

The IP multicast implies both routers and hosts. In IPv4, multicast is optional but almost all hosts and routers support it.  The mechanism that allows hosts to exchange some messages with routers is called `Internet Group Management Protocol` (IGMP).

The tasks of the router using IP multicast essentially consist of two main processes : multicast routing and multicast forwarding.

As explained above, the multicast routing is used to set up the distribution tree for a given multicast group by modifying the multicast routing tables. Those multicast routing tables can contain a list of multiple next hop addresses for a given table entry. When a router receives a new multicast packet, the router performs a lookup in the multicast routing table for a matching entry, as in unicast.  Afterwards, the router will forward one copy of the incoming packet to each next hop address in the matching routing table entry.


Overview
--------

As seen before with the three ways to send data, IP protocol has three types of addresses : unicast, broadcast and multicast. A unicast address is used to send a packet to a single known destination. Multicast is similar to a broadcast in the sense that it targets a number of machines on a network, but not all. When a broadcast is directed to all hosts on the network, a multicast is directed to a group of hosts. Hosts can choose whether they wish to join a multicast group whereas in broadcast, all hosts are part of the group.

A multicast group is composed of a set of hosts. The group is dynamic, every hosts can join or leave the group at any time. A source can emits packets in a group even if it is not part of it. Multicast traffic flows one-way from the source to the receivers, that why UDP protocol is used to transmit multicast packets (TCP congestion control is therefore unavailable). Multicast relies on IP protocol to transmit datagrams. A multicast datagram is delivered to destination group members with the same reliability as a standard unicast IP datagram. This means that multicast datagrams are not guaranteed to reach all members of a group or to arrive in the same order in which they were transmitted.

As you understood, a key principle in multicast are the multicast groups. Each groups have an IP multicast group address. If a source wants to emit multicast packet towards a group, it sends IP packets with the IP destination address set to the IP address of the group. Receivers that wants to receive packets from a group have to advertise their routers that they want to receive packets with destination the IP address of the group. 

A typical Multicast on an Ethernet network, using TCP/IP protocol, consists of three parts :
Ethernet multicast (layer 2), IP multicast (layer 3) and the mapping between these two.


Advantages and drawbacks
------------------------

**Advantages**

- `Improve the efficiency` by reducing server and CPU loads.
- `Optimize the performance` by eliminating traffic redundancy.
- `Distributed applications` by making multipoint communication possible.

**Drawbacks**

- `No control on the participation` to a multicast group by a source.
- `No identification or authentication` ; it has to be managed at the application level.


.. rubric:: References

.. [#funivsmulti] http://www.cisco.com

.. [#fmulticast] http://www.cs.virginia.edu/~itlab/book/pdf/Ch10_v1.pdf

