.. sectionauthor:: Hussein Bahmad

Flood and Prune Protocol
========================

The `flood and prune protocol` is also known as `reverse-path multicast algorithm`. The aim of this protocol is to be able to get the minimal distribution tree (shortest path tree) finding a way to traverse at most once each link in the whole network in each direction.

How does it work ?
------------------

When a given sender starts sending, the traffic is flooded out through the entire network. Regarding the routers, the traffic may arrive on different interfaces from several paths. The router will drop each incoming packet on the interface other than the one it uses when it sends unicast packet back to the source. And then the router will send a copy of packet out of each interface other than the one back to the source. (`Reverse path broadcast`)

Even if there is no receiver, the traffic will be received by many parts of the network. The router knows if there are receivers or not for a given multicast group via IGMP mechanism. During the "flood" step of the flood and prune algorithm, all the interfaces receive all the packets.  So the routers have to send a packet to all of its interfaces except the one where it has been received.

At the leaf multicast router, if there is no receiver, the router should send a `Prune` message back to the source to stop this unnecessary traffic because there is no receiver. The final distribution tree at the end should be the union of shortest paths from each receiver to a specific sender. But sending traffic everywhere using broadcast is obviously not an efficient approach, even less scalable.  All the routers that are not on the delivery tree need to store prune state. 

For example, if a group has one member in Belgium and one member in France and no member in Germany. The routers in Germany still receive some packet and to prevent from more packets arriving, the routers need always to hold prune state. This option works well using groups where the receivers are densely distributed. Despite its flaws, flood and prune protocols might be suitable for some networks.

.. figure:: imgs/flood-prune-protocol-example.png
   :align: center
   :scale: 80

   Example of Flood and Prune Protocol working [ffpprotdef]_

..


Disadvantages
-------------

 1. Large amount of unnecessary packets sending
 2. Need to maintain a state to prevent duplications


.. rubric:: References

.. [#ffpprotdef] http://media.packetlife.net/media/blog/attachments/131/PIM-DM.png
