=========================
Multiple choice questions
=========================

:task_id: mpls

.. sectionauthor:: David Sarkozi

.. question::
        :nb_pos: 4
        :nb_prop: 6
        
        1. In which of those areas brings MPLS improvements ?
        
        .. positive:: Quality of Service support
                
        .. positive:: Traffic Engineering
                
        .. positive:: Virtual Private Networks
                
        .. positive:: Multiprotocol support
                
        .. negative:: Border Gateway Protocol
                
        .. negative:: SCTP
                
        .. negative:: BGP decision process
                
        .. negative:: TCP
                
.. question::
        :nb_pos: 4
        :nb_prop: 8
        
        2. Which of the following (sequence of) bits are present in an MPLS packet header ?
        
        .. positive:: Label value
                
        .. positive:: Traffic Class field (experimental)
                
        .. positive:: Bottom of stack bit
                
        .. positive:: Time To Live (TTL) field
                
        .. negative:: Version
                
        .. negative:: Checksum
                
        .. negative:: Source Address
                
        .. negative:: Destination Address
                
        .. negative:: Fragment Offset
                
        .. negative:: Urgent Pointer
                
        .. negative:: Hop Limit
        
        .. negative:: Authentication field
                
.. question::
        :nb_pos: 2
        :nb_prop: 4
        
        3. What are the differences between Label and IP packet forwarding ?
        
        .. positive:: The size of the data structure on each node is smaller in MPLS.
        
        .. negative:: The size of the data structure on each node is bigger in MPLS.
                
        .. positive:: The computational cost of a network address lookup is much lower in MPLS.
        
        .. negative:: The computational cost of a network address lookup is much higher in MPLS.
                
        .. positive:: MPLS is better suited in high-bandwidth networks, performance-wise.
                
        .. negative:: MPLS is better suited in low-bandwidth networks, performance-wise.
                
.. question::
        :nb_pos: 1
        :nb_prop: 4
        
        4. Given the following network :
        
        .. tikz::
            :libs: positioning, matrix

            \tikzset{router/.style = {rectangle, draw, text centered, minimum height=2em}, }
            \tikzset{host/.style = {circle, draw, text centered, minimum height=2em}, }
            \node[host] (A) {A};
            \node[router, right of=A] (R1) {R1};
            \node[router, right=of R1] (R3) {R3};
            \node[router, right=of R3] (R5) {R5};
            \node[router, below=of R1] (R2) {R2};
            \node[router, below=of R3] (R4) {R4};
            \node[host, below of=R4] (C) {C};
            \node[host, right of=R5] (B) {B};


            \path[draw,thick]
            (A) edge (R1)
            (R1) edge (R2)
            (R3) edge (R1)
            (R2) edge (R4)
            (R4) edge (R3)
            (R4) edge (R5)
            (R3) edge (R5)
            (R4) edge (C)
            (R5) edge (B)
            (R2) edge (C);
            
        What forwarding tables would be valid to forward a packet,
        having [L1|L2|L3] as label stack, from A to C ?
        
        .. positive::
                +--------+---------+---------+-------------------+
                | Router | InPort  | InLabel | Nexthop Operation |
                +--------+---------+---------+-------------------+
                |   R1   |   L2    |  East   |        POP        |
                +        +---------+---------+-------------------+
                |        |   L3    |  Local  |        POP        |
                +--------+---------+---------+-------------------+
                |   R3   |   L1    |  South  |      SWAP(L4)     |
                +--------+---------+---------+-------------------+
                |   R4   |   L4    |  South  |        POP        |
                +--------+---------+---------+-------------------+
                
        .. negative::
                +--------+---------+---------+-------------------+
                | Router | InPort  | InLabel | Nexthop Operation |
                +--------+---------+---------+-------------------+
                |   R1   |   L1    |  East   |        POP        |
                +--------+---------+---------+-------------------+
                |        |   L2    |  Local  |   PUSH(L5); POP   |
                +   R3   +---------+---------+-------------------+
                |        |   L3    |  South  |        POP        |
                +--------+---------+---------+-------------------+
                |   R4   |   L5    |  South  |        POP        |
                +--------+---------+---------+-------------------+
                
                .. comment:: Did you correctly considered your label stack as being LIFO ?
                
        .. negative::
                +--------+---------+---------+-------------------+
                | Router | InPort  | InLabel | Nexthop Operation |
                +--------+---------+---------+-------------------+
                |   R1   |   L3    |  South  |        POP        |
                +--------+---------+---------+-------------------+
                |   R2   |   L2    |  East   |      SWAP(L3)     |
                +--------+---------+---------+-------------------+
                |   R3   |   L1    |  South  |      PUSH(L4)     |
                +--------+---------+---------+-------------------+
                |        |   L3    |  N-East |        POP        |
                +   R4   +---------+---------+-------------------+
                |        |   L4    |  South  |      POP; POP     |
                +--------+---------+---------+-------------------+
                |   R5   |   L1    |  East   |        POP        |
                +--------+---------+---------+-------------------+
                
                .. comment:: You actually forwarded the packet from A to B, not C.
                
        .. negative::
                +--------+---------+---------+-------------------+
                | Router | InPort  | InLabel | Nexthop Operation |
                +--------+---------+---------+-------------------+
                |        |   L1    |  East   |    PUSH(L1); POP  |
                +   R1   +---------+---------+-------------------+
                |        |   L3    |  East   |        POP        |
                +--------+---------+---------+-------------------+
                |        |   L1    |  North  |        POP        |
                +   R2   +---------+---------+-------------------+
                |        |   L2    |  East   |        POP        |
                +--------+---------+---------+-------------------+
                |        |   L1    |  South  |      SWAP(L1)     |
                +   R3   +---------+---------+-------------------+
                |        |   L2    |  South  |        POP        |
                +--------+---------+---------+-------------------+
                |        |   L1    |  West   |      PUSH(L1)     |
                +   R4   +---------+---------+-------------------+
                |        |   L4    |  South  |      POP; POP     |
                +--------+---------+---------+-------------------+
                |   R5   |   L1    |  East   |        POP        |
                +--------+---------+---------+-------------------+
                
                .. comment:: This provokes an infinite loop !
                
.. question::
        :nb_pos: 2
        :nb_prop: 4
        
        5. Compare the two ways to advertise label mappings :
        
        .. positive:: It is better to send LMs on all the links to decrease the CPU effort cost.
        
        .. negative:: It is better to send LMs only on the shortest path links to decrease the CPU effort cost.
        
        .. positive:: There are more LM messages in the network if we send LMs on all the links.
        
        .. negative:: There are more LM messages in the network if we send LMs only on the shortest path links.
        
        .. negative:: It is better to send LMs on all the links to decrease the memory consumption.
        
        .. positive:: It is better to send LMs only on the shortest path links to decrease the memory
                consumption.
        
        .. positive:: The network recovers faster in case of failure if LMs are sent across all the links.
        
        .. negative:: The network recovers faster in case of failure if LMs are sent only across the shortest
                path links.
