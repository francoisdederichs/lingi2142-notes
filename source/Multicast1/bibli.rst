.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Bibliography
==============

.. sectionauthor:: Syed Mohammad
.. sectionauthor:: Detaille Mattieu


.. [BG1992] Bertsekas, D., Gallager, G., `Data networks <http://books.google.com/books?id=FfpSAAAAMAAJ>`_, second edition, Prentice Hall, 1992
.. [CM2004] C. Metz and M. Tatipamula, `A look at native IPv6 multicast`, Internet Computing, IEEE, no. 4, 2004.
.. [MJ4] M. J. Handley, and J. Crowcroft, `Internet Multicast Today <http://www.cisco.com/web/about/ac123/ac147/ac174/ac198/about_cisco_ipj_archive_article09186a00800c851e.html>`_, The Internet Protocol Journal, vol. 2. No 4
.. [CISCO] Cisco, `IPV6 MLD Snooping <http://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst6500/ios/15-1SY/config_guide/sup2T/15_1_sy_swcg_2T/ipv6_mld_snooping.pdf>`_
.. [CB2000] C Diot, B N Levine, B Lyles, and H Kassem, `Deployment issues for the IP multicast service and architecture`, IEEE Network, 2000 vol. 14 (1) pp. 78-88
.. [CNP3] O Bonaventure, `Computer Networking : Principles, Protocols and Practice <http://cnp3book.info.ucl.ac.be/1st/html/>`_, 2012
.. [RFC20] Cerf, V., `ASCII format for network interchange`, :rfc:`20`, Oct. 1969
.. [RFC768] Postel, J., `User Datagram Protocol`, :rfc:`768`, Aug. 1980
.. [RFC789] Rosen, E., `Vulnerabilities of network control protocols: An example`, :rfc:`789`, July 1981
.. [RFC791] Postel, J., `Internet Protocol`, :rfc:`791`, Sep. 1981
.. [RFC792] Postel, J., `Internet Control Message Protocol`, :rfc:`792`, Sep. 1981
.. [RFC793] Postel, J., `Transmission Control Protocol`, :rfc:`793`, Sept. 1981
.. [RFC813] Clark, D., `Window and Acknowledgement Strategy in TCP`, :rfc:`813`, July 1982
.. [RFC819] Su, Z. and Postel, J., `Domain naming convention for Internet user applications`, :rfc:`819`, Aug. 1982
.. [RFC821] Postel, J., `Simple Mail Transfer Protocol`, :rfc:`821`, Aug. 1982
.. [RFC822] Crocker, D., `Standard for the format of ARPA Internet text messages, :rfc:`822`, Aug. 1982
.. [RFC826] Plummer, D., ``Ethernet Address Resolution Protocol: Or Converting Network Protocol Addresses to 48.bit Ethernet Address for Transmission on Ethernet Hardware`, :rfc:`826`, Nov. 1982
.. [RFC879] Postel, J., `TCP maximum segment size and related topics`, :rfc:`879`, Nov. 1983
.. [RFC893] Leffler, S. and Karels, M., `Trailer encapsulations`, :rfc:`893`, April 1984
.. [RFC894] Hornig, C., `A Standard for the Transmission of IP Datagrams over Ethernet Networks`, :rfc:`894`, April 1984
.. [RFC896] Nagle, J., `Congestion Control in IP/TCP Internetworks`, :rfc:`896`, Jan. 1984
.. [RFC952] Harrenstien, K. and Stahl, M. and Feinler, E., `DoD Internet host table specification`, :rfc:`952`, Oct. 1985
.. [RFC959] Postel, J. and Reynolds, J., `File Transfer Protocol`, :rfc:`959`, Oct. 1985
.. [RFC974] Partridge, C., `Mail routing and the domain system`, :rfc:`974`, Jan. 1986
.. [RFC1032] Stahl, M., `Domain administrators guide`, :rfc:`1032`, Nov. 1987
.. [RFC1035] Mockapteris, P., `Domain names - implementation and specification`, :rfc:`1035`, Nov. 1987
.. [RFC1042] Postel, J. and Reynolds, J., `Standard for the transmission of IP datagrams over IEEE 802 networks`, :rfc:`1042`, Feb. 1988
.. [RFC1055] Romkey, J., `Nonstandard for transmission of IP datagrams over serial lines: SLIP`, :rfc:`1055`, June 1988
.. [RFC1071] Braden, R., Borman D. and Partridge, C., `Computing the Internet checksum`, :rfc:`1071`, Sep. 1988
.. [RFC1122] Braden, R., `Requirements for Internet Hosts - Communication Layers`, :rfc:`1122`, Oct. 1989
.. [RFC1144] Jacobson, V., `Compressing TCP/IP Headers for Low-Speed Serial Links`, :rfc:`1144`, Feb. 1990
.. [RFC1149] Waitzman, D., `Standard for the transmission of IP datagrams on avian carriers`, :rfc:`1149`, Apr. 1990
.. [RFC1169] Cerf, V. and Mills, K., `Explaining the role of GOSIP`, :rfc:`1169`, Aug. 1990
.. [RFC1191] Mogul, J. and Deering, S., `Path MTU discovery`, :rfc:`1191`, Nov. 1990
.. [RFC1195] Callon, R., `Use of OSI IS-IS for routing in TCP/IP and dual environments`, :rfc:`1195`, Dec. 1990
.. [RFC1258] Kantor, B., `BSD Rlogin`, :rfc:`1258`, Sept. 1991
.. [RFC1321] Rivest, R., `The MD5 Message-Digest Algorithm`, :rfc:`1321`, April 1992
.. [RFC1323] Jacobson, V., Braden R. and Borman, D., `TCP Extensions for High Performance`, :rfc:`1323`, May 1992
.. [RFC1347] Callon, R., TCP and UDP with Bigger Addresses (TUBA), `A Simple Proposal for Internet Addressing and Routing`, :rfc:`1347`, June 1992
.. [RFC1518] Rekhter, Y. and Li, T., `An Architecture for IP Address Allocation with CIDR`, :rfc:`1518`, Sept. 1993
.. [RFC1519] Fuller V., Li T., Yu J. and Varadhan, K., `Classless Inter-Domain Routing (CIDR): an Address Assignment and Aggregation Strategy`, :rfc:`1519`, Sept. 1993
.. [RFC1542] Wimer, W., `Clarifications and Extensions for the Bootstrap Protocol`, :rfc:`1542`, Oct. 1993
.. [RFC1548] Simpson, W., `The Point-to-Point Protocol (PPP)`, :rfc:`1548`, Dec. 1993
.. [RFC1550] Bradner, S. and Mankin, A., `IP: Next Generation (IPng) White Paper Solicitation`, :rfc:`1550`, Dec. 1993
.. [RFC1561] Piscitello, D., `Use of ISO CLNP in TUBA Environments`, :rfc:`1561`, Dec. 1993
.. [RFC1621] Francis, P., `PIP Near-term architecture`, :rfc:`1621`, May 1994
.. [RFC1624] Risjsighani, A., `Computation of the Internet Checksum via Incremental Update`, :rfc:`1624`, May 1994
.. [RFC1631] Egevang K. and Francis, P., `The IP Network Address Translator (NAT)`, :rfc:`1631`, May 1994
.. [RFC1661] Simpson, W., `The Point-to-Point Protocol (PPP)`, :rfc:`1661`, Jul. 1994
.. [RFC1662] Simpson, W., `PPP in HDLC-like Framing`, :rfc:`1662`, July 1994
.. [RFC1710] Hinden, R., `Simple Internet Protocol Plus White Paper`, :rfc:`1710`, Oct. 1994
.. [RFC1738] Berners-Lee, T., Masinter, L., and McCahill M., `Uniform Resource Locators (URL)`, :rfc:`1738`, Dec. 1994
.. [RFC1752] Bradner, S. and Mankin, A., `The Recommendation for the IP Next Generation Protocol`, :rfc:`1752`, Jan. 1995
.. [RFC1812] Baker, F., `Requirements for IP Version 4 Routers`, :rfc:`1812`, June 1995
.. [RFC1819] Delgrossi, L., Berger, L., `Internet Stream Protocol Version 2 (ST2) Protocol Specification - Version ST2+`, :rfc:`1819`, Aug. 1995
.. [RFC1889] Schulzrinne H., Casner S., Frederick, R. and Jacobson, V., `RTP: A Transport Protocol for Real-Time Applications`, :rfc:`1889`, Jan. 1996
.. [RFC1896] Resnick P., Walker A., `The text/enriched MIME Content-type`, :rfc:`1896`, Feb. 1996
.. [RFC1918] Rekhter Y., Moskowitz B., Karrenberg D., de Groot G. and Lear, E., `Address Allocation for Private Internets`, :rfc:`1918`, Feb. 1996
.. [RFC1939] Myers, J. and Rose, M., `Post Office Protocol - Version 3`, :rfc:`1939`, May 1996
.. [RFC1945] Berners-Lee, T., Fielding, R. and Frystyk, H., `Hypertext Transfer Protocol -- HTTP/1.0`, :rfc:`1945`, May 1996
.. [RFC1948] Bellovin, S., `Defending Against Sequence Number Attacks`, :rfc:`1948`, May 1996
.. [RFC1951] Deutsch, P., `DEFLATE Compressed Data Format Specification version 1.3`, :rfc:`1951`, May 1996
.. [RFC1981] McCann, J., Deering, S. and Mogul, J., `Path MTU Discovery for IP version 6`, :rfc:`1981`, Aug. 1996
.. [RFC2003] Perkins, C., `IP Encapsulation within IP`, :rfc:`2003`, Oct. 1996
.. [RFC2018] Mathis, M., Mahdavi, J., Floyd, S. and Romanow, A., `TCP Selective Acknowledgment Options`, :rfc:`2018`, Oct. 1996
.. [RFC2045] Freed, N. and Borenstein, N., `Multipurpose Internet Mail Extensions (MIME) Part One: Format of Internet Message Bodies`, :rfc:`2045`, Nov. 1996
.. [RFC2046] Freed, N. and Borenstein, N., `Multipurpose Internet Mail Extensions (MIME) Part Two: Media Types`, :rfc:`2046`, Nov. 1996
.. [RFC2050] Hubbard, K. and Kosters, M. and Conrad, D. and Karrenberg, D. and Postel, J., `Internet Registry IP Allocation Guidelines`, :rfc:`2050`, Nov. 1996
.. [RFC2080] Malkin, G. and Minnear, R., `RIPng for IPv6`, :rfc:`2080`, Jan. 1997
.. [RFC2082] Baker, F. and Atkinson, R., `RIP-2 MD5 Authentication`, :rfc:`2082`, Jan. 1997
.. [RFC2131] Droms, R., `Dynamic Host Configuration Protocol`, :rfc:`2131`, March 1997
.. [RFC2140] Touch, J., `TCP Control Block Interdependence`, :rfc:`2140`, April 1997
.. [RFC2225] Laubach, M., Halpern, J., `Classical IP and ARP over ATM`, :rfc:`2225`, April 1998
.. [RFC2328] Moy, J., `OSPF Version 2`, :rfc:`2328`, April 1998
.. [RFC2332] Luciani, J. and Katz, D. and Piscitello, D. and Cole, B. and Doraswamy, N., `NBMA Next Hop Resolution Protocol (NHRP)`, :rfc:`2332`, April 1998
.. [RFC2364] Gross, G. and Kaycee, M. and Li, A. and Malis, A. and Stephens, J., `PPP Over AAL5`, :rfc:`2364`, July 1998
.. [RFC2368] Hoffman, P. and Masinter, L. and Zawinski, J., `The mailto URL scheme`, :rfc:`2368`, July 1998
.. [RFC2453] Malkin, G., `RIP Version 2`, :rfc:`2453`, Nov. 1998
.. [RFC2460] Deering S., Hinden, R., `Internet Protocol, Version 6 (IPv6) Specification`, :rfc:`2460`, Dec. 1998
.. [RFC2464] Crawford, M., `Transmission of IPv6 Packets over Ethernet Networks`, :rfc:`2464`, Dec. 1998
.. [RFC2507] Degermark, M. and Nordgren, B. and Pink, S., `IP Header Compression`, :rfc:`2507`, Feb. 1999
.. [RFC2516] Mamakos, L. and Lidl, K. and Evarts, J. and Carrel, J. and Simone, D. and Wheeler, R., `A Method for Transmitting PPP Over Ethernet (PPPoE)`, :rfc:`2516`, Feb. 1999
.. [RFC2581] Allman, M. and Paxson, V. and Stevens, W., `TCP Congestion Control`, :rfc:`2581`, April 1999
.. [RFC2616] Fielding, R. and Gettys, J. and Mogul, J. and Frystyk, H. and Masinter, L. and Leach, P. and Berners-Lee, T., `Hypertext Transfer Protocol -- HTTP/1.1`, :rfc:`2616`, June 1999
.. [RFC2617] Franks, J. and Hallam-Baker, P. and Hostetler, J. and Lawrence, S. and Leach, P. and Luotonen, A. and Stewart, L., `HTTP Authentication: Basic and Digest Access Authentication`, :rfc:`2617`, June 1999
.. [RFC2622] Alaettinoglu, C. and Villamizar, C. and Gerich, E. and Kessens, D. and Meyer, D. and Bates, T. and Karrenberg, D. and Terpstra, M., `Routing Policy Specification Language (RPSL)`, :rfc:`2622`, June 1999
.. [RFC2675] Tsirtsis, G. and Srisuresh, P., `Network Address Translation - Protocol Translation (NAT-PT)`, :rfc:`2766`, Feb. 2000
.. [RFC2854] Connolly, D. and Masinter, L., `The 'text/html' Media Type`, :rfc:`2854`, June 2000
.. [RFC2965] Kristol, D. and Montulli, L., `HTTP State Management Mechanism`, :rfc:`2965`, Oct. 2000
.. [RFC2988] Paxson, V. and Allman, M., `Computing TCP's Retransmission Timer`, :rfc:`2988`, Nov. 2000
.. [RFC2991] Thaler, D. and Hopps, C., `Multipath Issues in Unicast and Multicast Next-Hop Selection`, :rfc:`2991`, Nov. 2000
.. [RFC3021] Retana, A. and White, R. and Fuller, V. and McPherson, D., `Using 31-Bit Prefixes on IPv4 Point-to-Point Links`, :rfc:`3021`, Dec. 2000
.. [RFC3022] Srisuresh, P., Egevang, K., `Traditional IP Network Address Translator (Traditional NAT)`, :rfc:`3022`, Jan. 2001
.. [RFC3031] Rosen, E. and Viswanathan, A. and Callon, R., `Multiprotocol Label Switching Architecture`, :rfc:`3031`, Jan. 2001
.. [RFC3168] Ramakrishnan, K. and Floyd, S. and Black, D., `The Addition of Explicit Congestion Notification (ECN) to IP`, :rfc:`3168`, Sept. 2001
.. [RFC3243] Carpenter, B. and Brim, S., `Middleboxes: Taxonomy and Issues`, :rfc:`3234`, Feb. 2002
.. [RFC3235] Senie, D., `Network Address Translator (NAT)-Friendly Application Design Guidelines`, :rfc:`3235`, Jan. 2002
.. [RFC3309] Stone, J. and Stewart, R. and Otis, D., `Stream Control Transmission Protocol (SCTP) Checksum Change`, :rfc:`3309`, Sept. 2002
.. [RFC3315] Droms, R. and Bound, J. and Volz, B. and Lemon, T. and Perkins, C. and Carney, M., `Dynamic Host Configuration Protocol for IPv6 (DHCPv6)`, :rfc:`3315`, July 2003
.. [RFC3330] IANA, `Special-Use IPv4 Addresses`, :rfc:`3330`, Sept. 2002
.. [RFC3360] Floyd, S., `Inappropriate TCP Resets Considered Harmful`, :rfc:`3360`, Aug. 2002
.. [RFC3390] Allman, M. and Floyd, S. and Partridge, C., `Increasing TCP's Initial Window`, :rfc:`3390`, Oct. 2002
.. [RFC3490] Faltstrom, P. and Hoffman, P. and Costello, A., `Internationalizing Domain Names in Applications (IDNA)`, :rfc:`3490`, March 2003
.. [RFC3501] Crispin, M., `Internet Message Access Protocol - Version 4 rev1`, :rfc:`3501`, March 2003
.. [RFC3513] Hinden, R. and Deering, S., `Internet Protocol Version 6 (IPv6) Addressing Architecture`, :rfc:`3513`, April 2003
.. [RFC3596] Thomson, S. and Huitema, C. and  Ksinant, V. and Souissi, M., `DNS Extensions to Support IP Version 6`, :rfc:`3596`, October 2003
.. [RFC3748] Aboba, B. and Blunk, L. and Vollbrecht, J. and Carlson, J. and Levkowetz, H., `Extensible Authentication Protocol (EAP)`, :rfc:`3748`, June 2004
.. [RFC3819] Karn, P. and Bormann, C. and Fairhurst, G. and Grossman, D. and Ludwig, R. and Mahdavi, J. and Montenegro, G. and Touch, J. and Wood, L., `Advice for Internet Subnetwork Designers`, :rfc:`3819`, July 2004
.. [RFC3828] Larzon, L-A. and Degermark, M. and Pink, S. and Jonsson, L-E. and  Fairhurst, G., `The Lightweight User Datagram Protocol (UDP-Lite)`, :rfc:`3828`, July 2004
.. [RFC3927] Cheshire, S. and Aboba, B. and Guttman, E., `Dynamic Configuration of IPv4 Link-Local Addresses`, :rfc:`3927`, May 2005
.. [RFC3931] Lau, J. and Townsley, M. and Goyret, I., `Layer Two Tunneling Protocol - Version 3 (L2TPv3)`, :rfc:`3931`, March 2005
.. [RFC3971] Arkko, J. and Kempf, J. and Zill, B. and Nikander, P., `SEcure Neighbor Discovery (SEND)`, :rfc:`3971`, March 2005
.. [RFC3972] Aura, T., `Cryptographically Generated Addresses (CGA)`, :rfc:`3972`, March 2005
.. [RFC3986] Berners-Lee, T. and Fielding, R. and Masinter, L., `Uniform Resource Identifier (URI): Generic Syntax`, :rfc:`3986`, January 2005
.. [RFC4033] Arends, R. and Austein, R. and Larson, M. and Massey, D. and Rose, S., `DNS Security Introduction and Requirements`, :rfc:`4033`, March 2005
.. [RFC4193] Hinden, R. and Haberman, B., `Unique Local IPv6 Unicast Addresses`, :rfc:`4193`, Oct. 2005
.. [RFC4251] Ylonen, T. and Lonvick, C., `The Secure Shell (SSH) Protocol Architecture`, :rfc:`4251`, Jan. 2006
.. [RFC4264] Griffin, T. and Huston, G., `BGP Wedgies`, :rfc:`4264`, Nov. 2005
.. [RFC4271] Rekhter, Y. and Li, T. and Hares, S., `A Border Gateway Protocol 4 (BGP-4)`, :rfc:`4271`, Jan. 2006
.. [RFC4291] Hinden, R. and Deering, S., `IP Version 6 Addressing Architecture`, :rfc:`4291`, Feb. 2006
.. [RFC4301] Kent, S. and Seo, K., `Security Architecture for the Internet Protocol`, :rfc:`4301`, Dec. 2005
.. [RFC4302] Kent, S., `IP Authentication Header`, :rfc:`4302`, Dec. 2005
.. [RFC4303] Kent, S., `IP Encapsulating Security Payload (ESP)`, :rfc:`4303`, Dec. 2005
.. [RFC4340] Kohler, E. and Handley, M. and Floyd, S., `Datagram Congestion Control Protocol (DCCP)`, :rfc:`4340`, March 2006
.. [RFC4443] Conta, A. and Deering, S. and Gupta, M., `Internet Control Message Protocol (ICMPv6) for the Internet Protocol Version 6 (IPv6) Specification`, :rfc:`4443`, March 2006
.. [RFC4451] McPherson, D. and Gill, V., `BGP MULTI_EXIT_DISC (MED) Considerations`, :rfc:`4451`, March 2006
.. [RFC4456] Bates, T. and Chen, E. and Chandra, R., `BGP Route Reflection: An Alternative to Full Mesh Internal BGP (IBGP)`, :rfc:`4456`, April 2006
.. [RFC4614] Duke, M. and Braden, R. and Eddy, W. and Blanton, E., `A Roadmap for Transmission Control Protocol (TCP) Specification Documents`, :rfc:`4614`, Oct. 2006
.. [RFC4648] Josefsson, S., `The Base16, Base32, and Base64 Data Encodings`, :rfc:`4648`, Oct. 2006
.. [RFC4822] Atkinson, R. and Fanto, M., `RIPv2 Cryptographic Authentication`, :rfc:`4822`, Feb. 2007
.. [RFC4838] Cerf, V. and Burleigh, S. and Hooke, A. and Torgerson, L. and Durst, R. and Scott, K. and Fall, K. and Weiss, H., `Delay-Tolerant Networking Architecture`, :rfc:`4838`, April 2007
.. [RFC4861] Narten, T. and Nordmark, E. and Simpson, W. and Soliman, H.,`Neighbor Discovery for IP version 6 (IPv6)`, :rfc:`4861`, Sept. 2007
.. [RFC4862] Thomson, S. and Narten, T. and Jinmei, T., `IPv6 Stateless Address Autoconfiguration`, :rfc:`4862`, Sept. 2007
.. [RFC4870] Delany, M., `Domain-Based Email Authentication Using Public Keys Advertised in the DNS (DomainKeys)`, :rfc:`4870`, May 2007
.. [RFC4871] Allman, E. and Callas, J. and Delany, M. and Libbey, M. and Fenton, J. and Thomas, M., `DomainKeys Identified Mail (DKIM) Signatures`, :rfc:`4871`, May 2007
.. [RFC4941] Narten, T. and Draves, R. and Krishnan, S., `Privacy Extensions for Stateless Address Autoconfiguration in IPv6`, :rfc:`4941`, Sept. 2007
.. [RFC4944] Montenegro, G. and Kushalnagar, N. and Hui, J. and Culler, D., `Transmission of IPv6 Packets over IEEE 802.15.4 Networks`, :rfc:`4944`, Sept. 2007
.. [RFC4952] Klensin, J. and Ko, Y., `Overview and Framework for Internationalized Email`, :rfc:`4952`, July 2007
.. [RFC4953] Touch, J., `Defending TCP Against Spoofing Attacks`, :rfc:`4953`, July 2007
.. [RFC4954] Simeborski, R. and Melnikov, A., `SMTP Service Extension for Authentication`, :rfc:`4954`, July 2007
.. [RFC4963] Heffner, J. and Mathis, M. and Chandler, B., `IPv4 Reassembly Errors at High Data Rates`, :rfc:`4963`, July 2007
.. [RFC4966] Aoun, C. and Davies, E., `Reasons to Move the Network Address Translator - Protocol Translator (NAT-PT) to Historic Status`, :rfc:`4966`, July 2007
.. [RFC4987] Eddy, W., `TCP SYN Flooding Attacks and Common Mitigations`, :rfc:`4987`, Aug. 2007
.. [RFC5004] Chen, E. and Sangli, S., `Avoid BGP Best Path Transitions from One External to Another`, :rfc:`5004`, Sept. 2007
.. [RFC5065] Traina, P. and McPherson, D. and Scudder, J., `Autonomous System Confederations for BGP`, :rfc:`5065`, Aug. 2007
.. [RFC5068] Hutzler, C. and Crocker, D. and Resnick, P. and Allman, E. and Finch, T., `Email Submission Operations: Access and Accountability Requirements`, :rfc:`5068`, Nov. 2007
.. [RFC5072] Varada, S. and Haskins, D. and Allen, E., `IP Version 6 over PPP`, :rfc:`5072`, Sept. 2007 
.. [RFC5095] Abley, J. and Savola, P. and Neville-Neil, G., `Deprecation of Type 0 Routing Headers in IPv6`, :rfc:`5095`, Dec. 2007
.. [RFC5227] Cheshire, S., `IPv4 Address Conflict Detection`, :rfc:`5227`, July 2008
.. [RFC5234] Crocker, D. and Overell, P., `Augmented BNF for Syntax Specifications: ABNF`, :rfc:`5234`, Jan. 2008
.. [RFC5321] Klensin, J., `Simple Mail Transfer Protocol`, :rfc:`5321`, Oct. 2008
.. [RFC5322] Resnick, P., `Internet Message Format`, :rfc:`5322`, Oct. 2008
.. [RFC5340] Coltun, R. and Ferguson, D. and Moy, J. and Lindem, A., `OSPF for IPv6`, :rfc:`5340`, July 2008
.. [RFC5598] Crocker, D., `Internet Mail Architecture`, :rfc:`5598`, July 2009
.. [RFC5646] Phillips, A. and Davis, M., `Tags for Identifying Languages`, :rfc:`5646`, Sept. 2009
.. [RFC5681] Allman, M. and Paxson, V. and Blanton, E., `TCP congestion control`, :rfc:`5681`, Sept. 2009
.. [RFC5735] Cotton, M. and Vegoda, L., `Special Use IPv4 Addresses`, :rfc:`5735`, January 2010 
.. [RFC5795] Sandlund, K. and Pelletier, G. and Jonsson, L-E., `The RObust Header Compression (ROHC) Framework`, :rfc:`5795`, March 2010
.. [RFC6077] Papadimitriou, D. and Welzl, M. and Scharf, M. and Briscoe, B., `Open Research Issues in Internet Congestion Control`, :rfc:`6077`, February 2011
.. [RFC6068] Duerst, M., Masinter, L. and Zawinski, J., `The 'mailto' URI Scheme` , :rfc:`6068`, October 2010 
.. [RFC6144] Baker, F. and Li, X. and Bao, X. and Yin, K., `Framework for IPv4/IPv6 Translation`, :rfc:`6144`, April 2011
.. [RFC6265] Barth, A., `HTTP State Management Mechanism`, :rfc:`6265`, April 2011
.. [RFC6274] Gont, F., `Security Assessment of the Internet Protocol Version 4`, :rfc:`6274`, July 2011
