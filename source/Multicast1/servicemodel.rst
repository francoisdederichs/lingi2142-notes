.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Service Models
==============

.. sectionauthor:: Syed Mohammad
.. sectionauthor:: Detaille Mattieu


There exist two service models for IP multicast: Any-source multicast(ASM) and Source-specific multicast(SSM). The service models are transparent to the hosts and to the receivers(one does not need to know the identity or location of the other). To discover and join a group, the receiver will use the protocol IGMP over IPv4 and MLD over IPv6(details about these protocols will be given later).

Any-source multicast(ASM)
-------------------------
The ASM model correspond to a many-to-many transmission scheme(e.g: videoconferencing, P2P gaming,etc). A multicast group is only defined by its group address. Any host that has joined a multicast group is allowed to send packets to the mulcast group address. A host interested in receiving packets will simply initiate a 'join' message for the chosen multicast group address. In this case, the routers will forward packets to hosts based on IP adresses.


Source-specific multicast(SSM)
------------------------------
The SSM model correspond to a one-to-many transmission scheme(e.g: media broadcasting,etc). Only one host in the multicast network is allowed to send packets. The network correspond to a tree routed at the source host. All other hosts are receivers. A receiver can notify the network that he wants to receive packets by initiating a 'join' mesage with the the chosen multicast group address and the source IP adress. In this case, the routers will forward packets to hosts based on source IP adresses and the source port.

It is possible to simulate an ASM using multiple SSM source-routed trees. Nevertheless, the later model impose an important load on the network compare to the genuine ASM service, since far more structures(trees) need to be saved in the routers. Moreover, the discovery of the multiple sources by a receiver can burden the network since a multicast group in a SSM service is defined by both the group and source addresses and a receiver will need to join all the multicast groups of the multiple sources composing the simulated ASM . The simulated ASM with multiple SSM is only feasible for a restricted number source hosts.
