.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

IPv6 Multicast
==============

.. sectionauthor:: Syed Mohammad
.. sectionauthor:: Detaille Mattieu

The development of IP multicast was motivated by the need to find a bandwidth-efficient way to send packets simultaneously to multiple hosts. Beside IP multicast, there are two ways to send packets to mytiple host: repeated unicast transmission and broadcast. Nevertheless, both options suffer from scalability issues. Indeed, as the network to serve grows, both options will consume inefficiently the bandwith. Moreover, in the case of the broadcast, packets will be sent to all the host present in the network, whether the host are willing or not to receive such packets. IP multicast solve these issues by the use of multicast group address. 

A sender host will transmit a single packet with destination the multicast group address. The switches and routers will duplicate the packet and forward it to the receivers. The receivers willing to receive packets from a given multicast group only need to inform the network(the router they are connected to). The routers maintain a distribution tree for each multicast group they are related to and whose leaf are the receivers. The details about these tree structures shall be discussed later whit Multicast Routing Protocols, for now just remember that the most widely used protocol is Protocol Independant Multicast(PIM).IP multicast scales well because it doesn't need to have any prior knowledge of both the receiver's identities and the number of receivers.

IP multicast (as described in :rfc:`1112`, :rfc:`4604` and :rfc:`5771`) is supported by both IPv4 and IPv6. Even though multicast is fairly mature and well understood with IPv4, issues have arised due to the limited addressing space of IPv4(32-bits address). We know for a fact that IPv4 is no longer suited to respond to the expansion of networking applications and devices; this statement holds also for application exploiting multicast. The main issue with IPv4 concerning multicast is collision : in a large network, we might come accross a situation where two multicast group are given the same address; as a result, receivers from both group  will receive packets that they are not supposed to. IPv6 address (128-bits) solve the problem with its huge addressing space (:math:`3.4*10^{38}` unique IP addresses) and gives hope for further growth of the market of application using IP multicast.


.. note:: IP multicast & ISP

   Currently, IP multicast is not a service provider by ISP. Often, enterprises and TV-distribution companies have deployed multicasting for their own employees or customers. Why ISPs don't provide IP multicast? Actually, they don't know how to bill such a service. Moreover, multicast lack of security features. Eavesdropping and flooding (DoS) is possible because there is no management(anybody can join a group address and send data).


.. toctree::
   :maxdepth: 2

   Service Models <servicemodel>
   Group membership protocols for IP multicast <igmp>
   Multicast addressing  <addressing>
   Bibliography <bibli>
