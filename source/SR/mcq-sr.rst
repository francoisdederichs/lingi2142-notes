===============
Segment routing
===============


Multiple choice questions
-------------------------


:task_id: sr

.. sectionauthor:: Dederichs Francois, Klien Florent, Hardy Simon, Scheerens Noel

.. question::
        :nb_prop: 3
        :nb_pos: 1

        1. What type of segment completely ignores the shortest-path algorithm?

        .. negative:: Node Segments
        .. positive:: Adjacency Segments
        .. negative:: Anycast Segments

.. question::
        :nb_prop: 3
        :nb_pos: 1

        2. In the following figure, is it possible to use Node Segments to load-balance the traffic across all the links of the network from router A to router H? If yes, how?

        .. image:: pictures/SR-mcq-1.png


        .. negative:: Use a Node Segment with a label corresponding to D, and then another Node Segment with a label corresponding to H
        .. negative:: Use 5 successive Node Segments : one corresponding to B, then C, then E, then F, then G. 
        .. positive:: It's impossible using only Node Segments


.. question::
        :nb_prop: 3
        :nb_pos: 1

        3. In the following figure, is it possible to use Anycast Segments to load-balance the traffic across all the links of the network from router A to router H? If yes, how?

        .. image:: pictures/SR-mcq-1.png

        .. negative:: Use a label L on all of the intermediate routers (B/C/D/E/F/G) and put one Anycast Segment on this label. 
        .. positive:: Use a label L1 on routers B/C/D, another label L2 on routers E/F, and put two successive Anycast Segments on labels L1 then L2. 
        .. positive:: Use a label L1 on routers B/E, another label L2 on routers C/F/G, and put two successive Anycast Segments on labels L1 then L2. 
        .. negative:: It's not possible with only Anycast Segments. 

.. question::
        :nb_prop: 3
        :nb_pos: 2

        4. In the following figure, how would you use Segment Routing in order to follow the path in green?

        .. image:: pictures/SR-mcq-2.png

        .. positive:: Put a Node Segment on router C, then a Node Segment on router D, and then an Adjacency Segment on interface north-east of router D. 
        .. positive:: Use only 3 Adjacency Segments : on router A (interface south-east), router C (interface north-east), and then router D (interface north-east). 
            .. comment:: You must put a Node Segment before every Adjacency Segment. 
        .. negative:: Use a Node Segment on router D, and then an Adjacency Segment on interface north-east of D. 
        .. negative:: Use 2 Node Segments on router D and then on router E. 
        .. negative:: It's not possible to define such a path with Segment Routing. 
            .. comment:: Segment Routing with Node, Adjacency and Anycast Segments allows to define any path of any shape. 

.. question::
        :nb_prop: 3
        :nb_pos: 3

        5. In the following figure, how would you use Segment Routing in order to use the F-G edge (shown in green)?

        .. image:: pictures/SR-mcq-3.png


        .. positive:: Use a Node Segment on router F, then an Adjacency Segment on interface east of router F. 
        .. negative:: Use only 2 Adjacency Segments on interface south-east of router D, and then on interface east of router F. 
            .. comment:: You must put a Node Segment before every Adjacency Segment. 
        .. negative:: Use only one Adjacency Segment on interface east of router F. 
            .. comment:: You must put a Node Segment before every Adjacency Segment. 
        .. positive:: Use 2 Node Segments on routers F and G. 
        .. positive:: Use one Node Segment on router F. 

.. question::
        :nb_prop: 3
        :nb_pos: 3


        6. Which of the following sentences are correct? 

        .. negative:: Segment Routing using MPLS uses a HMAC in order to secure the label stack. 
            .. comment:: Only the IPv6 dataplane uses a HMAC. 
        .. negative:: The active segment in the IPv6 dataplane is the segment at the top of the stack. 
            .. comment:: It's the segment referenced by the pointer. 
        .. negative:: The Segment List of a packet in both MPLS and IPv6 dataplanes is constant throughout the path. 
            .. comment:: In MPLS, the labels are popped out of the stack so the packet size grows smaller along the path. 
        .. positive:: It's possible to make a path that loops in the network with Segment Routing. 
        .. positive:: Segment Routing with IPv6 uses a pointer to the current segment and doesn't remove segments from the list in order to save processing time. 
        .. positive:: With Segment Routing, an intermediate router can't modify the Segment List. 
            .. comment:: True, for obvious security reasons. 
