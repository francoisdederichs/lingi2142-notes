.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Differentiated services
-----------------------
.. sectionauthor:: nobody

.. todo:: 

   Explain the architectural principles of differentiated services and then the different types of services. 



Some references include 

 - :rfc:`2475` for the architecture (seel also :rfc:`3260`)
 - :rfc:`2474` for the DS field in the packet headers 
 - :rfc:`3246` for expedited forwarding
 - :rfc:`2597` for assured forwarding
 
