.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Integrated Services
-------------------
.. sectionauthor:: Hamaide Mickael and Gerard Kevin


============
Introduction
============
The creation of IntServ model was motivated by the needs of real time application such as video streaming, VoIP, ... 
In this model, clients will be able to require fine-grained control over flows such as :

 - guaranteed end-to-end delay
 - a minimum bandwidth capacity
 - a certain threshold amount of congestion losses

These services such as video streaming may require one more guarantees over the flow of data they send in order to avoid image tearing, audio distortion,... It also allows ISP's to dedicate unused bandwidth to different classes of traffic. Each class of traffic will receive a part of the available resources and to offer different guarantees to these flows. This is called controlled link sharing.
These possibilities were not included in the current best effort service that is deployed. The complete model is described in [:rfc:`1633`, :rfc:`2211`, :rfc:`2212`]. 

============
Architecture
============
Integrated services (IntServ) are divided into 4 main components that will act together in order to achieve Quality of Service.
These 4 components are the followings :

 - Reservation setup protocol
 - Control admission
 - Classifier
 - Packet scheduler

Reservation setup protocol
==========================
IntServ has to use some kind of setup protocol which sets the parameters of a flow and maintains flow specific state in the routers on the different paths.  It will mainly use the Resource ReserVation Protocol (RSVP) to achieve such result but other approaches like manual configuration or any kind of management protocol are also possible.

Control admission
=================
Once a network node receives all the specific parameters of a flow, the control admission has to make the decision to accept or reject this flow. The network element has to check if the flow is sustainable at all time considering other flows that may require some quality of service.

It has nothing to do with policing. Indeed, it only looks if the requested quality of service can be granted, compared to policing which only looks if packets are respecting the flow specifications.

The control admission can also modify the scheduler and classifier according to the flow statistics.

Classifier
========== 
The classifier is in charge of identifying the different flows and to map them into classes. All packets of a particular class are sharing the same amount of resources.

The identification mechanism is looking inside the header (and possibly other fields) of the network packets to detect the actual class the packet belongs to. It means that a class could be for instance all VoIP packets from a specific source to some destination.

Packet scheduler
================
The scheduler is implemented as a set of queues containing all the packets already spreaded into different classes. Since some classes have higher purposes and are more restrictive (e.g. video streaming), queues have also different priorities. IntServ does not force to implement any specific scheduler as long as it is able to  distinguish the different classes.

.. TODO:: put a schema of the architecture

=========================
Applications requirements
=========================
There are many kind of applications in this world, each one requiring some QoS dependant on their purpose. Few types of applications are stated below in order to give a more detailed idea of the service parameters.

Real-Time applications
======================
Real-Time applications are obviously time-sensitive. Most of these applications (e.g. VoIP, Internet radio...) are playback applications which means that they are receiving packet delayed by the network and trying to recompose the original file from these packets . In order to do this, they will need buffers to deal with delay and jitter brought by the network. The quality of service parameters required will most certainly be bandwidth (for performance reason), delay and jitter (i.e. an upper bound of these parameters) and loss.

Elastic applications
====================
Elastic applications do not require any quality of service. In fact, these applications are not time-sensitive and therefore, they do not care about the various quality parameters (mainly the delay). For instance, emails are part of these elastic applications since they wait for the other user to connect before being delivered. Unfortunately, real-time applications are often non-elastic.

Interactive applications
========================
Interactive applications are applications with sequences of interactions between endhosts (human-human, machine-human or machine-machine). For instance, IP-Telephone which needs delays and jitter parameters set to some maximum values in order to have an acceptable interaction between the hosts. Therefore they can require QoS such as bandwidth, delay, loss and jitter.

Noninteractive applications only needs bandwidth in order to keep some level of performance. Indeed, these applications do not need to interact with each other.


Tolerant applications
=====================
Intolerant applications do not work properly if a strict requirement is not respected. On the contrary, tolerant applications do not set strict requirements but ranges, however, once these requirements are violated, the application will not run properly.

Adaptive applications
=====================
Adaptive applications are applications which try to adapt to the condition of transmission and adapt the content of their packets to these conditions. For instance, some streaming websites adapt the quality of the video to the available bandwidth. Consequently, nonadaptive applications do not deal well with lower quality of service than expected.

========
Services
========
Three main services respecting the Integrated services goals are present nowadays. In the last two services, each flow requirements in terms of quality of service is characterized by a token bucket with a specific rate r and depth size b (and more parameters depending on the service). This token bucket allows the network nodes to check whether the flow is violating its requirements or not.

Null service
============
This service is basically using the reservation setup protocol to reserve a path between the end-hosts. However, the service does not offer any guarantees. 

Controlled load service
=======================
The goal of this service is to approximate best-effort service under unloaded conditions [:rfc:`2211`]. Moreover, we want to keep the quality of service whenever the network becomes overloaded. The traffic will mostly experience low delays and low loss.  

Therefore, the network elements only allocate bandwidth to flows requiring quality of service. The end-hosts initiating these flows must provide an estimation of the traffic (i.e. the token bucket) which will be used by network nodes to setup necessary resources. Additionally, they have to provide a peak rate, a minimum policed unit m and a maximum packet size M.

The minimum policed unit is used for packets whose size is smaller than m. These packets are subsequently virtually using the minimum size m for the token bucket. However, the packets whose size is bigger than M are simply dropped.

Moreover, during a period of time T, we are not supposed to receive more than :math:`r*T+b` bytes of data. All unconforming packets have to follow some rules[:rfc:`2211`]:

 - All conforming flows must remain unaffected
 - The best-effort traffic should not be disturbed by the excess of unconforming flows
 - The excess traffic must be forwarded on a best-effort basis


Guaranteed service
==================
The guaranteed service provides an upper bound for the end-to-end delay as well as a minimum bandwidth. This maximum delay depends on the propagation and transmission delay (which are both physical delays) and the serialization delay (which depends on the processing of the packets in the buffers). This upper bound needs to be large in order to cover every possible case. Indeed, the maximal delay cannot be violated since it is required by the applications. 

The serialization delay is closely related to the token bucket. Therefore, the token bucket must be well-defined for every flow requiring the guaranteed service. It is also related to delays specific to the network node itself due to the resources allocated, for instance, the size of the buffer dedicated to the flow.

The end-to-end delay upper bound can be calculated as [:rfc:`2212`]:

.. math:: (\frac{b-M}{R}*\frac{p-R}{p-r})+\frac{M+Ctot}{R}+Dtot

where the token bucket of the flow is (r,b), M is the maximal datagram size, p is the peak rate. R, C and D are node-related and respectively correspond to the bandwidth allocated, the error rate-dependant queueing delay, and the error rate-independant queueing delay. Ctot and Dtot are just the sum of the queueing delays along the path.

Guaranteed service supports policing and shaping. While policing is only comparing the traffic to the specifications of the flow at the end of the network, the shaping can be done at every node on the path and tries force the traffic to conform to its specifications. During a period of time T, the traffic cannot exceed :math:`M+min(pT, rT+b-M)` bytes of data. The excess of traffic should be processed on a best-effort basis by the policy. In the network, the exceeding traffic is reshaped to the token bucket characteristics (this is done by a buffer). All the exceeding traffic cannot necessarely be reshaped, so the non-conforming packets should also be processed as best-effort. However, even if defined like such in theory, the exceeding packets have a high probability of being dropped.

==================
Scalability issues
==================
The issue of IntServ is that it must heavily process every packet. Indeed, it must check to which flow it belongs to, furthermore, it has to check if it conforms to the flow specifications and also it must check the reservations every time it receives one. It also inherits all the issues of the reservation protocol (if it is used).

Reference

 - :rfc:`1633` for architectural principles (see also David D. Clark, Scott Shenker, and Lixia Zhang. 1992. Supporting real-time applications in an Integrated Services Packet Network: architecture and mechanism. In Conference proceedings on Communications architectures & protocols (SIGCOMM '92), David Oran (Ed.). ACM, New York, NY, USA, 14-26. DOI=10.1145/144179.144199 http://doi.acm.org/10.1145/144179.144199 )
 - :rfc:`2211` for Controlled Load
 - :rfc:`2212` for Guaranteed Service
 - MOHAMED A. EL-GENDY, ABHIJIT BOSE AND KANG G. SHIN : Evolution of the Internet QoS and Support for Soft Real-Time Applications, P    ROCEEDINGS OF THE IEEE, VOL. 91, NO. 7, p1086-1104, JULY 2003.
